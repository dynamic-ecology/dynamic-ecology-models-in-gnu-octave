## GNU Octave ports of the models in "Dynamic Ecology - an introduction to
## the art of simulating trophic dynamics" by Flynn, K. (2018).
## Copyright (C) 2020  Ekin Akoglu and Kevin J. Flynn
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <https://www.gnu.org/licenses/>.

function xdot = func_allometric_quota_npz(t, x)

global NC_Phy1
global NC_Phy2
global u_Phy1
global u_Phy2
global u_Zoo
global uN_Phy1
global NCu_Phy1
global NCu_Phy2
global AEmin
global AEmax
global AEqual
global AEquan
global AEC_Zoo
global ingC_Zoo
global ingCmax_Zoo
global AEC
global AEN
global GGEC
global GGEN
global palat_Phy1
global palat_Phy2
global Enc_Phy1
global Enc_Phy2
global ingPhy1C
global ingPhy2C
global PR_Phy1
global PR_Phy2
global CRC_Phy1
global CRC_Phy2
global NCPhy_Zoo
global Stoich_con
global XSC

# Dilution
dil = 0.05;       # Dilution rate (d-1)

# Phytoplankton 1 parameters
ktAM_Phy1	=	14;   # Half saturation constant for ammonium transport (ugN L-1)
umax_Phy1 = 0.5;  # Maximum C-specific growth rate (gC (gC)-1 d-1)
NCmin_Phy1 = 0.07;# Minimum NC_Phy (gN (gC)-1)
NCmax_Phy1 = 0.15;# Maximum NC_Phy (gN (gC)-1)
kQN_Phy1 = 10;    # KQ for N-quota (dl)

# Phytoplankton 2 parameters
ktAM_Phy2	=	70;   # Half saturation constant for ammonium transport (ugN L-1)
umax_Phy2 = 0.8;  # Maximum C-specific growth rate (gC (gC)-1 d-1)
NCmin_Phy2 = 0.05;# Minimum NC_Phy (gN (gC)-1)
NCmax_Phy2 = 0.15;# Maximum NC_Phy (gN (gC)-1)
kQN_Phy2 = 10;    # KQ for N-quota (dl)

# Zooplankton parameters
AEmax = 0.6;      # Maximum AE for N (dl)
AEmin = 0.2;      # Maximum AE for N (dl)
BR_Zoo = 0.1;     # Basal respiration rate as a proportion of umax_Zoo (dl)
kAE = 1.00e+03;   # Constant for control of AE in response to prey quality (dl)
kGTT = 100;       # Curve control for density dependant inefficiency (ugC L-1)
minAE_mult = 1;   # Minimum AEC scalar for density dependant inefficiency (dl)
NC_Zoo = 0.2;     # N:C of zooplankton (gN (gC)-1)
SDA = 0.3;        # Specific dynamic action (dl)
tox_Phy1 = 0;     # Toxicity factor for Phy1; 0 not toxic (dl)
tox_Phy2 = 1.1;   # Toxicity factor for Phy2 (dl)
Optimal_CR = 1;   # Proportion of prey of optimal characteristics captured by starved Zoo (dl) 
umax_Zoo = 0.693; # Zooplankton maximum growth rate (d-1)

# Encounter sub-model variables
a = 0.216;        # Parameter for derivation of C-cell content for protist of a given volume (dl)
b = 0.939;        # Parameter for derivation of C-cell content for protist of a given volume (dl)
r_Phy1 = 2.5;     # Radius of Phy1 cell (um)
r_Phy2 = 5;       # Radius of Phy2 cell (um)
r_Zoo = 50;       # Radius of Zoo cell (um)
w = 0;            # Root-mean-squared turbulence (m s-1)

# Prey optimality sub-model variables
relMaxPrey = 0.3; # Maximum prey:pred (dl)
relMinPrey = 0.025; # Minimum prey:pred (dl)
relOpPrey = 0.2;  # Optimal prey:pred (dl) 

# Voided materials parameters
NCmax = 0.3;      # Maximum mass ratio of N:C which could be attained in the organic form (gN (gC)-1)

## Auxiliaries
# C content of Phy1 (pgC cell-1)
Ccell_Phy1 = a * (4 / 3 * pi * (r_Phy1)^3)^b;

# C content of Phy2 (pgC cell-1)
Ccell_Phy2 = a * (4 / 3 * pi * (r_Phy2)^3)^b;

# C content of Zoo (pgC cell-1)
Ccell_Zoo = a * (4 / 3 * pi * (r_Zoo)^3)^b;

# Cell abundance of Phy1 (Phy1 cells m-3)
nos_Phy1 = 10^9 * x(3) / Ccell_Phy1;

# Cell abundance of Phy2 (Phy2 cells m-3)
nos_Phy2 = 10^9 * x(5) / Ccell_Phy2;

# Speed of motility of Phy1 (m s-1)
v_Phy1 = (10^-6) * (38.542 * (r_Phy1 * 2)^0.5424);

# Speed of motility of Phy2 (m s-1)
v_Phy2 = (10^-6) * (38.542 * (r_Phy2 * 2)^0.5424);

# Speed of motility of Zoo (m s-1)
v_Zoo = (10^-6) * (38.542 * (r_Zoo * 2)^0.5424);

# Encounter rate of a cell of Phy1 by a cell of Zoo (Phy1 Zoo-1 d-1)
Enc_Phy1(t - 1) = (24 * 60 * 60) * pi * (r_Phy1 / 1E6 + r_Zoo / 1E6)^2 * nos_Phy1 * (v_Phy1^2 + 3 * v_Zoo^2 + 4 * w^2) * ((v_Zoo^2 + w^2)^-0.5) * 3^-1;

# Encounter rate of a cell of Phy2 by a cell of Zoo (Phy2 Zoo-1 d-1)
Enc_Phy2(t - 1) = (24 * 60 * 60) * pi * (r_Phy2 / 1E6 + r_Zoo / 1E6)^2 * nos_Phy2 * (v_Phy2^2 + 3 * v_Zoo^2 + 4 * w^2) * (v_Zoo^2 + w^2)^-0.5 * 3^-1;

# prey:pred for Phy1 (dl)
rel_Phy1 = r_Phy1 / r_Zoo;

# prey:pred for Phy2 (dl)
rel_Phy2 = r_Phy2 / r_Zoo;

# Prey handling index for Phy1, taking into account the prey:pred relative size (dl)
if relMaxPrey > rel_Phy1 && rel_Phy1 > relMinPrey
  if rel_Phy1 < relOpPrey
    PR_Phy1(t - 1) = (rel_Phy1 - relMinPrey) / (relOpPrey - relMinPrey);
  else
    PR_Phy1(t - 1) = (relMaxPrey - rel_Phy1) / (relMaxPrey - relOpPrey);
  endif
else
  PR_Phy1(t - 1) = 0;
endif

# Prey handling index for Phy2, taking into account the prey:pred relative size (dl)
if relMaxPrey > rel_Phy2 && rel_Phy2 > relMinPrey
  if rel_Phy2 < relOpPrey
    PR_Phy2(t - 1) = (rel_Phy2 - relMinPrey) / (relOpPrey - relMinPrey);
  else
    PR_Phy2(t - 1) = (relMaxPrey - rel_Phy2) / (relMaxPrey - relOpPrey);
  endif
else
  PR_Phy2(t - 1) = 0;
endif

# Nutrient exchange (ugN L-1 d-1)
in_out_Am = dil * (280 - x(1));

# Washout of N_Phy1 (ugN L-1 d-1)
outN_Phy1 = x(2) * dil;

# Washout of C_Phy1 (ugC L-1 d-1)
outC_Phy1 = x(3) * dil;

# Washout of N_Phy2 (ugN L-1 d-1)
outN_Phy2 = x(4) * dil;

# Washout of C_Phy2 (ugC L-1 d-1)
outC_Phy2 = x(5) * dil;

# Phytoplankton 1 N:C quota (gN (gC)-1)
NC_Phy1(t - 1) = x(2) / x(3);

# Phytoplankton 2 N:C quota (gN (gC)-1)
NC_Phy2(t - 1) = x(4) / x(5);

# Quotient for N status (dl)
NCu_Phy1(t - 1) = ((1 + kQN_Phy1) * (NC_Phy1(t - 1) - NCmin_Phy1)) / ((NC_Phy1(t - 1) - NCmin_Phy1) + kQN_Phy1 * (NCmax_Phy1 - NCmin_Phy1));

# Quotient for N status (dl)
NCu_Phy2(t - 1) = ((1 + kQN_Phy2) * (NC_Phy2(t - 1) - NCmin_Phy2)) / ((NC_Phy2(t - 1) - NCmin_Phy2) + kQN_Phy2 * (NCmax_Phy2 - NCmin_Phy2));

# C-specific growth rate controlled by N:C quota for Phy1 (gC (gC)-1 d-1)
u_Phy1(t - 1) = umax_Phy1 * NCu_Phy1(t - 1);

# C-specific growth rate controlled by N:C quota for Phy2 (gC (gC)-1 d-1)
u_Phy2(t - 1) = umax_Phy2 * NCu_Phy2(t - 1);

# Maximum C-specific N transport rate for Phy1 (gN (gC)-1 d-1)
TNmax_Phy1 = umax_Phy1 * NCmax_Phy1;

# Maximum C-specific N transport rate for Phy2 (gN (gC)-1 d-1)
TNmax_Phy2 = umax_Phy2 * NCmax_Phy2;

# Phytoplankton C-specific N transport rate for Phy1 (gN (gC)-1 d-1)
NCt_Phy1 = TNmax_Phy1 * x(1) / (x(1) + ktAM_Phy1);

# Phytoplankton C-specific N transport rate fpr Phy 2(gN (gC)-1 d-1)
NCt_Phy2 = TNmax_Phy2 * x(1) / (x(1) + ktAM_Phy2);

# N-specific growth rate for Phy1 (gN (gN)-1 d-1)
uN_Phy1(t - 1) = NCt_Phy1 / NC_Phy1(t -1);

# N-specific growth rate for Phy2 (gN (gN)-1 d-1)
uN_Phy2(t - 1) = NCt_Phy2 / NC_Phy2(t -1);

# Phytoplankton-1 population uptake of ammonium-N (ugN L-1 d-1)
Nup_Phy1 = x(3) * NCt_Phy1;

# Phytoplankton-2 population uptake of ammonium-N (ugN L-1 d-1)
Nup_Phy2 = x(5) * NCt_Phy2;

# Total consumption of ammonium (ugN L-1 d-1)
Am_up = Nup_Phy1 + Nup_Phy2;

# Growth rate in phytoplankton1-C (ugC L-1 d-1)
groC_Phy1 = x(3) * u_Phy1(t - 1);

# Growth rate in phytoplankton2-C (ugC L-1 d-1)
groC_Phy2 = x(5) * u_Phy2(t - 1);

# Palatability index (0 not palatable) (dl)
palat_Phy1(t - 1) = (NCu_Phy1(t - 1) + 1.0e-6)^tox_Phy1;

# Palatability index (0 not palatable) (dl)
palat_Phy2(t - 1) = (NCu_Phy2(t - 1) + 1.0e-6)^tox_Phy2;

# Potential capture of Phy1 taking into account all factors (Phy1 Zoo-1 d-1)
CR_Phy1 = Enc_Phy1(t - 1) * PR_Phy1(t - 1) * palat_Phy1(t - 1) * Optimal_CR;

# Potential capture of Phy2 taking into account all factors (Phy2 Zoo-1 d-1)
CR_Phy2 = Enc_Phy2(t - 1) * PR_Phy2(t - 1) * palat_Phy2(t - 1) * Optimal_CR;

# Potential C-specific ingestion Phy1 (gC (gC)-1 d-1)
CRC_Phy1(t - 1) = CR_Phy1 * Ccell_Phy1 / Ccell_Zoo;

# Potential C-specific ingestion Phy2 (gC (gC)-1 d-1)
CRC_Phy2(t - 1) = CR_Phy2 * Ccell_Phy2 / Ccell_Zoo;

# Sum of potential prey capture rate (gC (gC)-1 d-1)
SCRC = CRC_Phy1(t - 1) + CRC_Phy2(t - 1);

# N:C of incoming food (gN (gC)-1)
ingNC(t - 1) = (CRC_Phy1(t - 1) * NC_Phy1(t - 1) + CRC_Phy2(t - 1) * NC_Phy2(t - 1)) / SCRC;

# Ratio of NC in prey compared to predator (dl)
NCPhy_Zoo(t - 1) = ingNC(t -1) / NC_Zoo;

# Selection of release of N related to difference in food to consumer N:C (dl)
Stoich_con(t - 1) = min(NCPhy_Zoo(t - 1), 1);

# AEC scalar for density dependant inefficiency (dl)
AEquan(t - 1) = (1 - minAE_mult) * (1 - (x(3) + x(5)) / (x(3) + x(5) + kGTT)) + minAE_mult;

# Efficiency parameter for assimilation (dl)
AEqual(t - 1)  = AEmin + (AEmax - AEmin) * Stoich_con(t - 1) / (Stoich_con(t - 1) + kAE) * (1 + kAE);

# Operational AE for C (dl)
AEC_Zoo(t - 1) = Stoich_con(t - 1) * AEqual(t - 1)  * AEquan(t - 1);

# Zooplankton basal respiration rate (d-1)
BR = umax_Zoo * BR_Zoo;

# Maximum ingestion rate by zooplankton (gC (gC)-1 d-1)
ingCmax_Zoo(t -1) = (umax_Zoo * (1 + SDA) + BR) / AEC_Zoo(t - 1);

# Satiation control constant (gC (gC)-1 d-1)
KI = ingCmax_Zoo(t -1) / 4;

# Ingestion rate of prey into zooplankton (gC (gC)-1 d-1)
ingC_Zoo(t - 1) = min(ingCmax_Zoo(t - 1) * SCRC / (SCRC + KI), SCRC);

# Ingestion rate of Phy1 by Zoo (gC (gC)-1 d-1)
ingPhy1C(t - 1) = ingC_Zoo(t - 1) * CRC_Phy1(t - 1) / SCRC;

# Ingestion rate of Phy2 by Zoo (gC (gC)-1 d-1)
ingPhy2C(t - 1) = ingC_Zoo(t - 1) * CRC_Phy2(t - 1) / SCRC;

# C available for support of respiration (gC (gC)-1 d-1)
if Stoich_con(t - 1) < 1
  XSC(t - 1) = AEqual(t - 1)  * ingC_Zoo(t - 1) * (1 - Stoich_con(t - 1));
else
  XSC(t - 1) = 0;
endif

# Basal respiration that is met bu respiration of excess C in diet (gC (gC)-1 d-1)
if BR <= XSC(t - 1)
  BRi = BR;
else
  BRi = XSC(t - 1);
endif

# Balance of basal respiration that cannot be met from dietary excees C (gC (gC)-1 d-1)
BRb = BR - BRi;

# Grazing upon phytoplankton-1 population (ugC L-1 d-1)
grazC_Phy1 = x(6) * ingPhy1C(t - 1);

# Grazing upon phytoplankton-2 population (ugC L-1 d-1)
grazC_Phy2 = x(6) * ingPhy2C(t - 1);

# Grazing upon phytoplankton-1 population in terms of N (ugN L-1 d-1)
grazN_Phy1 = x(6) * ingPhy1C(t - 1) * NC_Phy1(t -1);

# Grazing upon phytoplankton-2 population in terms of N (ugN L-1 d-1)
grazN_Phy2 = x(6) * ingPhy2C(t - 1) * NC_Phy2(t -1);

# Assimilation rate into zooplankton (gC (gC)-1 d-1)
assC_Zoo = AEC_Zoo(t - 1)  * ingC_Zoo(t -1);

# Assimilation of C into zooplankton population biomass (ugC L-1)
assC = x(6) * assC_Zoo;

# Zooplankton respiration rate (gC (gC)-1 d-1)
resC_Zoo = BRb + assC_Zoo * SDA; 

# Zooplankton population respiration (ugC L-1 d-1)
respC = x(6) * resC_Zoo;

# Zooplankton growth rate (gC (gC)-1 d-1)
u_Zoo(t - 1) = assC_Zoo - resC_Zoo;

# Amount of N initially in the organic form to be voided to maintain constant predator N:C (gN (gC)-1 d-1)
XSassN = ingC_Zoo(t - 1) * ingNC(t - 1) - assC_Zoo * NC_Zoo;

# AE in terms of C (dl)
AEC(t - 1) = assC_Zoo / ingC_Zoo(t - 1);

# AR in terms of N (dl)
AEN(t - 1) = (assC_Zoo * NC_Zoo) / (ingC_Zoo(t - 1) * ingNC(t - 1));

# Voiding of C by zooplankton (gC (gC)-1 d-1)
voidC_Zoo = ingC_Zoo(t - 1) - assC_Zoo - BRi;

# Population rate of C voiding (ugC L-1 d-1)
voidC = x(6) * voidC_Zoo;

# Voiding of N by zooplankton (gN (gC)-1 d-1)
if (XSassN / voidC_Zoo) > NCmax
  voidN_Zoo = voidC_Zoo * NCmax;
else
  voidN_Zoo = XSassN;
endif

# Population rate of N voiding (ugN L-1 d-1)
voidN = x(6) * voidN_Zoo;

# Zooplankton ammonium regeneration (gN (gC)-1 d-1)
DINr = resC_Zoo * NC_Zoo + XSassN - voidN_Zoo;

# GGE in terms of C (dl)
GGEC(t - 1) = (ingC_Zoo(t -1) - voidC_Zoo - resC_Zoo - BRi) / ingC_Zoo(t -1);

# GGE in terms of N (dl)
GGEN(t - 1) = (ingC_Zoo(t -1) * ingNC(t -1) - voidN_Zoo - DINr) / (ingC_Zoo(t -1) * ingNC(t -1));

# Zooplankton population regeneration of ammonium (ugN L-1 d-1)
reg_Am = x(6) * DINr;

# Washout of voided C (ugC L-1 d-1)
out_VOC = x(8) * dil;

# Washout of voided N (ugN L-1 d-1)
out_VON = x(7) * dil;

# Washout of zooplankton biomass (ugC L-1 d-1)
outC_Zoo = x(6) * dil;

## State equations
# Ammonium
xdot(1, 1) = in_out_Am + reg_Am - Am_up;

# Phytoplankton1-N
xdot(1, 2) = Nup_Phy1 - grazN_Phy1 - outN_Phy1;

# Phytoplankton1-C
xdot(1, 3) = groC_Phy1 - grazC_Phy1 - outC_Phy1;

# Phytoplankton2-N
xdot(1, 4) = Nup_Phy2 - grazN_Phy2 - outN_Phy2;

# Phytoplankton2-C
xdot(1, 5) = groC_Phy2 - grazC_Phy2 - outC_Phy2;

# Zooplankton-C
xdot(1, 6) = assC - respC - outC_Zoo;

# VON
xdot(1, 7) = voidN - out_VON;

# VOC
xdot(1, 8) = voidC - out_VOC;

# System
xdot(1, 9) = xdot(1, 1) + xdot(1, 2) + xdot(1, 4) + xdot(1, 6) * NC_Zoo + xdot(1, 7);

endfunction

