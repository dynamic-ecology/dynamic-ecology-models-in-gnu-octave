## GNU Octave ports of the models in "Dynamic Ecology - an introduction to
## the art of simulating trophic dynamics" by Flynn, K. (2018).
## Copyright (C) 2020  Ekin Akoglu and Kevin J. Flynn
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <https://www.gnu.org/licenses/>.

function xdot = func_light(t, x)

global Wm2_enter
global Wm2
global coszen
global sol_deca
global Noon_Wm2
global Noon_coszen
global d_len
global d_len_frac
global deg_1
global E_enter
global dusk
global sol_dec_deg
global r_vec
global deg_hr
global tspan
global Rate_1
global Rate_2

# Parameters
solar_const = 1368; # Solar constant irradiance (W m-2 = J/m2/s); maximum irradiance to Earth from the sun (W m-2)
sw_JD = 1;          # Switch; if 0 then date is fixed to set_JD; if 1 then increment with TIME (dl)
atmos_clar = 0.55;  # Corrects for atmospheric clarity (varies with lat, long  & JD) (dl)
con_fact = 4.57;    # Converts W m-2 to PAR umol m-2 s-1 for cloud-less sky with sun (dl)
lat =	47;           # Latitude (degrees-north)
set_JD = 0;         # Required fixed date (see sw_JD) (d)

## Auxiliaries
# Current time as fraction of day (dl)
frac_day =	tspan(t - 1) - floor(tspan(t - 1));

# Current time as fraction of day in hours (hrs)
t24	=	24 * frac_day;

# Degrees of hour angle away from noon (default 12:00) (dl)
deg_hr(t - 1) =	abs(12 - t24) * 15;

# Hour angle radians (rad)
r_hr =	deg_hr(t - 1) * pi / 180;

if sw_JD == 1 
  mult1 = 1;
else
  mult1 = 0;
endif
if sw_JD == 0
  mult2 = 1;
else
  mult2 = 0;
endif
# Julian day; note the 10d offset (starting the year on 22nd December) (d)
JD = mult1 * 365 * (((tspan(t - 1) + 10) / 365) - floor(((tspan(t - 1) + 10) / 365))) + mult2 * set_JD;

# Solar declination angle (rad)
sol_deca(t - 1)	=	23.45 * sin(2 * pi * (284 + JD) * 0.00274) * pi / 180;

# Latitude in radians (rad)
r_lat =	lat * pi / 180;

# Cosine of zenith angle; positive values only accepted (dl)
coszen(t - 1) =	max(sin(r_lat) * sin(sol_deca(t - 1)) + cos(r_lat) * cos(sol_deca(t - 1)) * cos(r_hr), 0);

# Angle the sun makes with the vertical (solar zenith angle) (rad)
theta1 =	acos(coszen(t - 1));

# Intermediate #1 in day length calculator (dl)
d_cal1 = -1 * tan(r_lat) * tan(sol_deca(t - 1));

if d_cal1 > -1
  mult1 = 1;
else
  mult1 = 0;
endif
if d_cal1 <= 1
  mult2 = 1;
else
  mult2 = 0;
endif
if d_cal1 <= -1
  mult3 = 1;
else
  mult3 = 0;
endif
if d_cal1 > 1
  mult4 = 1;
else
  mult4 = 0;
endif
# Intermediate #2 in day length calculator (dl)
d_cal2 = d_cal1 * mult1 * mult2 + -1 * mult3 + 1 * mult4;

# Day length at current Julian date (hr)
d_len(t - 1)	=	(2 * acos(d_cal2) * 12 / pi);

# Day length at current Julian date (d)
d_len_frac(t - 1) =	d_len(t - 1) / 24;

# Time of dusk (d)
dusk(t - 1) =	(0.5 + d_len_frac(t - 1) / 2);

# Angle the sun makes with the vertical (solar zenith angle)
deg_1(t - 1)	=	theta1 * deg2rad(1.0);

# Proportion of light incident with the water surface that is just under the surface, accounting for reflectance (dl)
E_enter(t - 1) =	1 - (1.15e-06 * deg_1(t - 1)^3 - 69.1340e-06 * deg_1(t - 1)^2 + 0.001 * deg_1(t - 1) + 0.0187);

# Photon m-2 s-1 PFD just under surface (umol)
nat_PFD	=	x(1) * con_fact;

# Daily irradiance (kJ m-2 d-1) 
kJ_m2_day	=	x(2) * 86400 / 1000;

# Value of coszen at noon (hence COS(0) at end of definition) (dl)
Noon_coszen(t - 1)	=	max(sin(r_lat) * sin(sol_deca(t - 1)) + cos(r_lat) * cos(sol_deca(t - 1)) * cos(0), 0);

# Earth radius vector
r_vec(t - 1)	=	1 / (1 + 0.033 * cos(2 * pi * JD * 0.00274))^0.5;

# Maximum irradiance (at noon) on this Julian date (W m-2)
Noon_Wm2(t - 1) =	solar_const / r_vec(t - 1) / r_vec(t - 1) * Noon_coszen(t - 1);

if coszen(t - 1) > 0
  mult1 = 1;
else
  mult1 = 0;
endif
# Irradiance at given hour and day; W m-2 [W = J s-1; i.e. J/m2/s] (W m-2)
Wm2(t - 1)	=	solar_const / r_vec(t - 1) / r_vec(t - 1) * coszen(t - 1) * mult1;

# Light actually entering water (just under surface), accounting for reflectance (W m-2)
Wm2_enter(t - 1)	=	Wm2(t - 1) * E_enter(t - 1) * atmos_clar;

# Intermediate calc
Rate_1(t - 1) =	Wm2_enter(t - 1);

# Intermediate calc; to average over 1 time unit (day)
if t < 10
  Rate_2(t - 1) = 0;
else
  Rate_2(t - 1) =	Rate_1(t - 9);
endif

# Sets time for 2nd year to zero at t = 365
simtime	=	tspan(t - 1) - 365;

# Solar declination angle (degrees)
sol_dec_deg(t - 1)	=	sol_deca(t - 1) * deg2rad(1.0);

## State equations
# Cumulative dose
xdot(1, 1) = Wm2_enter(t - 1);

# Average daily irradiance
xdot(1, 2) = Rate_1(t - 1) - Rate_2(t - 1);

endfunction

