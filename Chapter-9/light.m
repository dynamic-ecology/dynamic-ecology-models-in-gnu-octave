## GNU Octave ports of the models in "Dynamic Ecology - an introduction to
## the art of simulating trophic dynamics" by Flynn, K. (2018).
## Copyright (C) 2020  Ekin Akoglu and Kevin J. Flynn
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <https://www.gnu.org/licenses/>.

clear;

global Wm2_enter
global Wm2
global coszen
global sol_deca
global Noon_Wm2
global Noon_coszen
global d_len
global d_len_frac
global deg_1
global E_enter
global dusk
global sol_dec_deg
global r_vec
global deg_hr
global tspan
global Rate_1
global Rate_2

# Simulation time frame
t0 = 0;       # start time
tfinal = 365; # end time
stepsize = 0.015625;
tspan = (t0:stepsize:tfinal); # time span

# Preallocate global arrays for speed
Wm2_enter = zeros(1, length(tspan)-1);
Wm2 = zeros(1, length(tspan)-1);
coszen = zeros(1, length(tspan)-1);
sol_deca = zeros(1, length(tspan)-1);
Noon_Wm2 = zeros(1, length(tspan)-1);
Noon_coszen = zeros(1, length(tspan)-1);
d_len = zeros(1, length(tspan)-1);
d_len_frac = zeros(1, length(tspan)-1);
deg_1 = zeros(1, length(tspan)-1);
E_enter = zeros(1, length(tspan)-1);
dusk = zeros(1, length(tspan)-1);
sol_dec_deg = zeros(1, length(tspan)-1);
r_vec = zeros(1, length(tspan)-1);
deg_hr = zeros(1, length(tspan)-1);
Rate_1 = zeros(1, length(tspan)-1);
Rate_2 = zeros(1, length(tspan)-1);

# Initial conditions
cum_MJ_m2 = 0;    # Cummulative dose (MJ m-2)
DAY_avg_W_m2 = 0; # Average daily irradiance (Wm-2)
# Initial conditions array
x0 = [cum_MJ_m2 DAY_avg_W_m2];

# Simulate
y = solver(@func_light, tspan, stepsize, x0);

# Plot the results
h = figure;
set(h, 'Position', [0   50   900   950]);
set(h, 'PaperPositionMode', 'auto');         

subplot(5, 3, 1);
plot(tspan(2:end), Wm2, 'r', tspan(2:end), Wm2_enter, 'g');
set(gca,'FontSize',12);
xlim([0 365]);
xlabel('Time (d)', 'FontSize', 12);
ylabel('W m^{-2}', 'FontSize', 12);
hleg = legend('Wm2', 'Wm2\_enter');
set(hleg, 'FontSize', 8);

subplot(5, 3, 2);
plot(tspan(2:end), dusk', 'r');
set(gca,'FontSize',12);
xlim([0 365]);
ylim([0 0.9]);
xlabel('Time (d)', 'FontSize', 12);
ylabel('dusk', 'FontSize', 12);

subplot(5, 3, 3);
plot(tspan(2:end), Noon_coszen, 'r');
set(gca,'FontSize',12);
xlim([0 365]);
xlabel('Time (d)', 'FontSize', 12);
ylabel('Noon\_coszen', 'FontSize', 12);

subplot(5, 3, 4);
plot(tspan(2:end), d_len_frac, 'r');
set(gca,'FontSize',12);
xlim([0 365]);
ylim([0 1.0]);
xlabel('Time (d)', 'FontSize', 12);
ylabel('d\_len\_frac (rad)', 'FontSize', 12);

subplot(5, 3, 5);
plot(tspan(2:end), d_len, 'r');
set(gca,'FontSize',12);
xlim([0 365]);
ylim([0 25]);
xlabel('Time (d)', 'FontSize', 12);
ylabel('d\_len (rad)', 'FontSize', 12);

subplot(5, 3, 6);
plot(tspan(2:end), coszen, 'r');
set(gca,'FontSize',12);
xlim([0 365]);
xlabel('Time (d)', 'FontSize', 12);
ylabel('coszen', 'FontSize', 12);

subplot(5, 3, 7);
plot(tspan(2:end), sol_deca, 'r');
set(gca,'FontSize',12);
xlim([0 365]);
xlabel('Time (d)', 'FontSize', 12);
ylabel('sol\_deca', 'FontSize', 12);

subplot(5, 3, 8);
plot(tspan(2:end), Noon_Wm2, 'r');
set(gca,'FontSize',12);
xlim([0 365]);
xlabel('Time (d)', 'FontSize', 12);
ylabel('Noon\_Wm2', 'FontSize', 12);

subplot(5, 3, 9);
plot(tspan(2:end), sol_dec_deg, 'r');
set(gca,'FontSize',12);
xlim([0 365]);
xlabel('Time (d)', 'FontSize', 12);
ylabel('sol\_dec\_deg', 'FontSize', 12);

subplot(5, 3, 10);
plot(tspan(2:end), E_enter, 'r');
set(gca,'FontSize',12);
xlim([0 365]);
ylim([0 1.0]);
xlabel('Time (d)', 'FontSize', 12);
ylabel('E\_enter', 'FontSize', 12);

subplot(5, 3, 11);
plot(tspan(2:end), deg_1, 'r');
set(gca,'FontSize',12);
xlim([0 365]);
xlabel('Time (d)', 'FontSize', 12);
ylabel('deg\_1', 'FontSize', 12);

subplot(5, 3, 12);
plot(tspan(2:end), r_vec, 'r');
set(gca,'FontSize',12);
xlim([0 365]);
xlabel('Time (d)', 'FontSize', 12);
ylabel('r\_vec', 'FontSize', 12);

subplot(5, 3, 14);
plot(tspan(2:end), deg_hr, 'r');
set(gca,'FontSize',12);
xlim([0 365]);
xlabel('Time (d)', 'FontSize', 12);
ylabel('deg\_hr', 'FontSize', 12);

print(h, 'Chapter-9-Light.png', '-dpng', '-color');