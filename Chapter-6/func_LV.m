## GNU Octave ports of the models in "Dynamic Ecology - an introduction to
## the art of simulating trophic dynamics" by Flynn, K. (2018).
## Copyright (C) 2020  Ekin Akoglu and Kevin J. Flynn
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <https://www.gnu.org/licenses/>.

function xdot = func_LV(t, x)

# Prey parameters
k1 = 0.693; # Prey-specific growth rate (d-1)
k2 = 0.5;   # Predator-specific prey loss constant (Pred-1 d-1)

# Predator parameters
k3 = 0.2; # Prey-specific predator growth rate (Prey-1 d-1)
k4 = 0.4; # Predator-specific predator loss rate (d-1)

## Auxiliaries
# Prey gain (Prey d-1)
gro_prey = k1 * x(1);

# Prey loss (Prey d-1)
death_prey = k2 * x(1) * x(2);

# Predator gain (Pred d-1)
gro_pred = k3 * x(1) * x(2);

# Predator loss (Pred d-1)
death_pred = k4 * x(2);

## State equations
# Prey
xdot(1, 1) = gro_prey - death_prey;

# Predator
xdot(1, 2) = gro_pred - death_pred;

endfunction