## GNU Octave ports of the models in "Dynamic Ecology - an introduction to
## the art of simulating trophic dynamics" by Flynn, K. (2018).
## Copyright (C) 2020  Ekin Akoglu and Kevin J. Flynn
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <https://www.gnu.org/licenses/>.

function xdot = func_harvest(t, x)

global u_Phy
global dil
global har_dil
global umax_Phy
global tspan

# Ammonium parameters
dil = 0.2;       # Dilution rate (L L-1 d-1)
har_f = 20;      # Frequency of harvesting (d)
har_pc = 0.95;   # Proportion harvested at frequency of har_f (dl)
ext_Am = 100;    # Concentration of Am in external reservoir (ugN L-1)
Pause_time = 20; # Time between pauses (d)

# Phytoplankton parameters
umax_Phy = 0.693; # Phytoplankton maximum N-specific growth rate (gN (gN)-1 d-1)
kAm_Phy = 14;     # Half saturation constant for u_Phy (ugN L-1)

## Auxiliaries
if tspan(t) > 0 
  mult1 = 1;
else
  mult1 = 0;
endif
if isequal(mod(tspan(t), har_f), 0)
  mult2 = 1;
else
  mult2 = 0;
endif
# Harvesting dilution rate (d-1)
har_dil(t - 1) = mult1 * mult2 * har_pc / 0.0625;

# Total dilution rate (d-1)
time_dil = dil + har_dil(t - 1);

# Inflow of ext_AM from reservoir (ugN L-1 d-1)
in_Am = ext_Am * time_dil;

# Outflow of AM from culture vessel (ugN L-1 d-1)
out_Am = x(1) * time_dil;

# Phytoplankton N-specific growth rate (gN (gN)-1 d-1)
u_Phy(t - 1)=	umax_Phy * x(1) / (x(1) + kAm_Phy);

# Phytoplankton population growth rate (ugN L-1 d-1)
gro_Phy = u_Phy(t - 1) * x(2);

# Outflow of Phy from culture vessel (ugN L-1 d-1)
out_Phy = x(2) * time_dil;

## State equations
# Ammonium
xdot(1, 1) = in_Am - out_Am - gro_Phy;

# Phytoplankton
xdot(1, 2) = gro_Phy - out_Phy;

# System
xdot(1, 3) = xdot(1, 1) + xdot(1, 2);

endfunction

