## GNU Octave ports of the models in "Dynamic Ecology - an introduction to
## the art of simulating trophic dynamics" by Flynn, K. (2018).
## Copyright (C) 2020  Ekin Akoglu and Kevin J. Flynn
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <https://www.gnu.org/licenses/>.

function xdot = func_simple_predprey(t, x)

global u_Phy
global u_Zoo

# Phytoplankton parameters
kAm_Phy	=	14;     # Half saturation constant for u_Phy (ugN L-1)
umax_Phy = 0.693; # Phytoplankton maximum N-specific growth rate (gN (gN)-1 d-1) 

# Zooplankton parameters
umax_Zoo = 1;     # Maximum specific growth rate of the zooplankton (gN (gN)-1 d-1)
kPhy_Zoo = 42;    # Half saturation constant for ingN_Zoo (ugN L-1)
thresPhy = 0.014; # Threshold for predation (ugN L-1)
BR_Zoo = 0.1;     # Index of basal (catabolic) respiration (dl)
AEN_Zoo = 0.6;    # Assimilation efficiency for N (dl)
SDA = 0.3;        # Specific dynamic action (anabolic respiration cost for assimilating N, gN/gN) 

## Auxiliaries
# Phytoplankton N-specific growth rate (gN (gN)-1 d-1)
u_Phy(t - 1)=	umax_Phy * x(1) / (x(1) + kAm_Phy);

# Phytoplankton population growth rate (ugN L-1 d-1)
gro_Phy = x(2) * u_Phy(t - 1);

# Ingestion rate with inclusion of threshold control (gN (gN)-1 d-1)
ingNmax_Zoo = (umax_Zoo * ( 1 + BR_Zoo)) / (AEN_Zoo * (1 - SDA));

# Maximum ingestion rate (gN (gN)-1 d-1)
if x(2) > thresPhy
  ingPhy_Zoo = ingNmax_Zoo * (x(2) - thresPhy) / (x(2) - thresPhy + kPhy_Zoo);
else
  ingPhy_Zoo = 0;
endif

# Zooplankton N-specific growth rate (gN (gN)-1 d-1)
u_Zoo(t - 1) = ingPhy_Zoo * AEN_Zoo * (1 - SDA) - (umax_Zoo * BR_Zoo);

# Zooplankton assimilation rate (gN (gN)-1 d-1)
assN_Zoo = ingPhy_Zoo * AEN_Zoo;

# Zooplankton N-specific regeneration rate (gN (gN)-1 d-1)
regN_Zoo = (umax_Zoo * BR_Zoo) + assN_Zoo * SDA;

# Zooplankton population ingestion rate (ugN L-1 d-1)
ing_Zoo = x(3) * ingPhy_Zoo;

# Zooplankton population N-regeneration rate (ugN L-1 d-1)
reg_Zoo = x(3) * regN_Zoo;

# Zooplankton population N-voiding rate (ugN L-1 d-1)
void_Zoo = x(3) * ingPhy_Zoo * (1 - AEN_Zoo);

## State equations
# Ammonium
xdot(1, 1) = -gro_Phy + reg_Zoo + void_Zoo;

# Phytoplankton
xdot(1, 2) = gro_Phy - ing_Zoo;

# Zooplankton
xdot(1, 3) = ing_Zoo - reg_Zoo - void_Zoo;

# System
xdot(1, 4) = xdot(1, 1) + xdot(1, 2) + xdot(1, 3);

endfunction

