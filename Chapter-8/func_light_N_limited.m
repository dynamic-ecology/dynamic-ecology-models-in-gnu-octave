## GNU Octave ports of the models in "Dynamic Ecology - an introduction to
## the art of simulating trophic dynamics" by Flynn, K. (2018).
## Copyright (C) 2020  Ekin Akoglu and Kevin J. Flynn
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <https://www.gnu.org/licenses/>.

function xdot = func_light_N_limited(t, x)

global att_tot
global Nu
global PSrel
global PSqz
global attco_Phy
global attco_W

# Physical parameters
attco_W = 0.05; # Absorbance coefficient for growth medium (water, m-1)
PFD = 500;      # Surface irradiance (umol photon m-2 s-1)
z = 10;         # Water (optical) depth (m)

# Chl-related parameters
abco_Chl = 0.02;     # Light absorbance coefficient for chlorophyll (m2 (mgChl)-1)
alpha_Chl = 7.00e-6; # Slope of Chl-specific PE curve ((m2 g-1 chl.a)*(gC umol-1 photon))

# Phytoplankton parameters
umax_Phy = 0.693; # Phytoplankton maximum N-specific growth rate (gN (gN)-1 d-1)
kAm_Phy = 14;     # Half saturation constant for Am limitation (ugN L-1)
BR_Phy = 0.05;    # Scaler for basal respiration rate (dl)
ChlC_Phy = 0.06;  # Mass ratio content of chlorophyll:C in the phytoplankton (gChl (gC)-1)
NC_Phy = 0.15;    # Mass ratio content of N-biomass:C in the phytoplankton (gN (gC)-1)

## Auxiliaries
# Phytoplankton-N specific coefficient for light absorbance (m2 (mgN)-1)
abco_PhyN = abco_Chl * ChlC_Phy / NC_Phy;

# Attenuation coefficient to phytoplankton N-biomass (m-1)
attco_Phy(t - 1) = abco_PhyN * x(2);

# Total attenuation (dl)
att_tot(t - 1) = z * (attco_W + attco_Phy(t - 1));

# Negative exponent of total attenuation (dl)
exatt = exp(-att_tot(t - 1));

# Maximum gross photosynthetic rate required to enable u_Phy=umax_Phy (d-1)
PSmax = umax_Phy * (1 + BR_Phy);

# Quotient for N-status (dl)
Nu(t - 1) = x(1) / (x(1) + kAm_Phy);

# Maximum photosynthetic rate down-regulated by nutrient stress (d-1)
PSqmax = PSmax * Nu(t - 1);

# Specific slope of PE curve ((m2)*(umol-1 photon))
alpha_u = alpha_Chl * ChlC_Phy;

# Intermediate in depth-integrated photosynthesis rate (dl)
pytq = (alpha_u * PFD * 24 * 60 * 60) / PSqmax;

# Phytoplankton N-specific growth rate (d-1)
PSqz(t - 1) = PSqmax * (log(pytq + sqrt(1 + pytq^2)) - log(pytq * exatt + sqrt(1 + (pytq * exatt)^2))) / att_tot(t - 1);

# Quotient for relative rate of PS (dl)
PSrel(t - 1) = PSqz(t - 1) / PSmax;

# Net growth rate (d-1)
u_Phy =	PSqz(t - 1) - umax_Phy * BR_Phy;

# Phytoplankton population growth rate (ugN L-1 d-1)
gro_Phy = u_Phy * x(2);

## State equations
# Ammonium
xdot(1, 1) = -gro_Phy;

# Phytoplankton
xdot(1, 2) = gro_Phy;

# System
xdot(1, 3) = xdot(1, 1) + xdot(1, 2);

endfunction

