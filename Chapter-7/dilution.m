## GNU Octave ports of the models in "Dynamic Ecology - an introduction to
## the art of simulating trophic dynamics" by Flynn, K. (2018).
## Copyright (C) 2020  Ekin Akoglu and Kevin J. Flynn
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <https://www.gnu.org/licenses/>.

clear;

global netin_Am
global u_Phy
global out_Phy
global dil
global umax_Phy

# Simulation time frame
t0 = 0;      # start time
tfinal = 20; # end time
stepsize = 0.0625;
tspan = (t0:stepsize:tfinal); # time span

# Preallocate global arrays for speed
netin_Am = zeros(1, length(tspan)-1);
u_Phy = zeros(1, length(tspan)-1);
out_Phy = zeros(1, length(tspan)-1);

# Initial conditions
Am = 99;         # Ammonium-N (ugN L-1)
Phy = 1;         # Phytoplankton biomass-N (ugN L-1)
sysN = Am + Phy; # System N-balance (ugN L-1)
# Initial conditions array
x0 = [Am Phy sysN];

# Simulate
y = solver(@func_dilution, tspan, stepsize, x0);

# Plot the results
h = figure;

subplot(2, 2, 1);
plot(tspan, y(:,1), 'r', tspan, y(:,2), 'g', tspan, y(:,3), 'b');
set(gca,'FontSize',12);
xlabel('Time (d)', 'FontSize', 12);
ylabel('\mugN L^{-1}', 'FontSize', 12);
hleg = legend('Am', 'Phy', 'sysN', 'location', 'east');
set(hleg, 'FontSize', 8);

subplot(2, 2, 2);
plot(tspan(2:end), repmat(dil, 1, length(tspan)-1), 'r', tspan(2:end), u_Phy', 'g', tspan(2:end), repmat(umax_Phy, 1, length(tspan)-1), 'b');
set(gca,'FontSize',12);
xlabel('Time (d)', 'FontSize', 12);
ylabel('d^{-1}', 'FontSize', 12);
hleg = legend('dil', 'u\_Phy', 'umax\_Phy', 'location', 'east');
set(hleg, 'FontSize', 8);

subplot(2, 2, 3);
plot(y(2:end,1), u_Phy', 'k');
set(gca,'FontSize',12);
xlabel('Am', 'FontSize', 12);
ylabel('u\_Phy', 'FontSize', 12);

subplot(2, 2, 4);
plot(tspan(2:end), netin_Am, 'r', tspan(2:end), out_Phy, 'g');
set(gca,'FontSize',12);
xlabel('Time (d)', 'FontSize', 12);
ylabel('\mugN L^{-1} d^{-1}', 'FontSize', 12);
hleg = legend('netin\_Am', 'out\_Phy', 'location', 'east');
set(hleg, 'FontSize', 8);

print(h, 'Chapter-7-Dilution.png', '-dpng', '-color');