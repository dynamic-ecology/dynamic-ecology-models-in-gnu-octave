## GNU Octave ports of the models in "Dynamic Ecology - an introduction to
## the art of simulating trophic dynamics" by Flynn, K. (2018).
## Copyright (C) 2020  Ekin Akoglu and Kevin J. Flynn
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Runge-Kutta 4th order ODE solver

function y = rk4(fun, tspan, stepsize, x0)
  
  y = zeros(size(tspan, 2), size(x0, 2));
  y(1, :) = x0;
  step = 1;
  
  for t = 2:size(tspan, 2)
    
    xdot1 = fun(t, y(step, :));
    xdot2 = fun(t + stepsize/2, y(step, :) + (stepsize/2) * xdot1);
    xdot3 = fun(t + stepsize/2, y(step, :) + (stepsize/2) * xdot2);
    xdot4 = fun(t + stepsize, y(step, :) + stepsize * xdot3);
    y(t, :) = y(step, :) + (stepsize / 6) * (xdot1 + 2 * xdot2 + 2 * xdot3 + xdot4);
    
    step = step + 1;
  endfor
  
  
endfunction
