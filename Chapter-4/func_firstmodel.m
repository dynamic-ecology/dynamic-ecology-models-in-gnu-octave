## GNU Octave ports of the models in "Dynamic Ecology - an introduction to
## the art of simulating trophic dynamics" by Flynn, K. (2018).
## Copyright (C) 2020  Ekin Akoglu and Kevin J. Flynn
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <https://www.gnu.org/licenses/>.

function xdot = func_firstmodel(t, x)

## Parameters
kAm_Phy	=	14;     # Half saturation constant for u_Phy (ugN L-1)
umax_Phy = 0.693; # Phytoplankton maximum N-specific growth rate (gN (gN)-1 d-1) 

## Auxiliaries
# Phytoplankton N-specific growth rate (gN (gN)-1 d-1)
u_Phy	=	umax_Phy * x(1) / (x(1) + kAm_Phy);

# Phytoplankton population growth rate (ugN L-1 d-1)
gro_Phy = x(2) * u_Phy; 

## State equations
# Ammonium
xdot(1, 1) = -gro_Phy;

# Phytoplankton
xdot(1, 2) = gro_Phy;

# System
xdot(1, 3) = xdot(1, 1) + xdot(1, 2);

endfunction

