## GNU Octave ports of the models in "Dynamic Ecology - an introduction to
## the art of simulating trophic dynamics" by Flynn, K. (2018).
## Copyright (C) 2020  Ekin Akoglu and Kevin J. Flynn
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <https://www.gnu.org/licenses/>.

clear;

# Simulation time frame
t0 = 0;      # start time
tfinal = 20; # end time
# Change stepsize below for the exercise in book: 1.0, 0.5, 0.25 or 0.125
stepsize = 1.0;
tspan = (t0:stepsize:tfinal); # time span

# Initial conditions
Phy =	1;          # Phytoplankton biomass-N (ugN L-1) 
Am = 100;         # Ammonium-N (ugN L-1)
sysN	= Am + Phy; # System N-balance (ugN L-1)
# Initial conditions array
x0 = [Am, Phy, sysN];

# Simulate
y = solver(@func_firstmodel, tspan, stepsize, x0);

# Plot the results
h = figure;

plot(tspan, y(:, 3), 'r', tspan, y(:, 1), 'g', tspan, y(:, 2), 'b');
set(gca,'FontSize',12);
xlabel('Time (d)', 'FontSize', 12);
ylabel('\mugN L^{-1}', 'FontSize', 12);
legend('sysN', 'Am', 'Phy');
figurename = ['Chapter-4-First-Model-TS-' num2str(stepsize), 'd.png'];
print(h, figurename, '-dpng', '-color');