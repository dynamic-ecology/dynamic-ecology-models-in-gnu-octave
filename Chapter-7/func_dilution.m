## GNU Octave ports of the models in "Dynamic Ecology - an introduction to
## the art of simulating trophic dynamics" by Flynn, K. (2018).
## Copyright (C) 2020  Ekin Akoglu and Kevin J. Flynn
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <https://www.gnu.org/licenses/>.

function xdot = func_dilution(t, x)

global netin_Am
global u_Phy
global out_Phy
global dil
global umax_Phy

# Ammonium parameters
dil = 0.2;    # Dilution rate (L L-1 d-1)
ext_Am = 100; # Concentration of Am in external reservoir (ugN L-1)
Pause_t = 10; # Time between pauses (d)

# Phytoplankton parameters
umax_Phy = 0.693; # Phytoplankton maximum N-specific growth rate (gN (gN)-1 d-1)
kAm_Phy = 14;     # Half saturation constant for u_Phy (ugN L-1)

## Auxiliaries
# Inflow of ext_AM from reservoir (ugN L-1 d-1)
in_Am = ext_Am * dil;

# Outflow of AM from culture vessel (ugN L-1 d-1)
out_Am = x(1) * dil;

# Net input of AM (ugN L-1 d-1)
netin_Am(t - 1) = dil * (ext_Am - x(1));

# Phytoplankton N-specific growth rate (gN (gN)-1 d-1)
u_Phy(t - 1)=	umax_Phy * x(1) / (x(1) + kAm_Phy);

# Phytoplankton population growth rate (ugN L-1 d-1)
gro_Phy = u_Phy(t - 1) * x(2);

# Outflow of Phy from culture vessel (ugN L-1 d-1)
out_Phy(t - 1) = x(2) * dil;

## State equations
# Ammonium
xdot(1, 1) = in_Am - out_Am - gro_Phy;

# Phytoplankton
xdot(1, 2) = gro_Phy - out_Phy(t - 1);

# System
xdot(1, 3) = xdot(1, 1) + xdot(1, 2);

endfunction

