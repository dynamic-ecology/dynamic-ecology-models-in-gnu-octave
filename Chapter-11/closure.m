## GNU Octave ports of the models in "Dynamic Ecology - an introduction to
## the art of simulating trophic dynamics" by Flynn, K. (2018).
## Copyright (C) 2020  Ekin Akoglu and Kevin J. Flynn
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <https://www.gnu.org/licenses/>.

clear;

global u_Phy
global u_Zoo

# Simulation time frame
t0 = 0;        # start time
tfinal = 300;  # end time
stepsize = 0.0625;
tspan = (t0:stepsize:tfinal); # time span

# Preallocate global arrays for speed
u_Phy = zeros(1, length(tspan)-1);
u_Zoo = zeros(1, length(tspan)-1);

# Initial conditions
Phy =	1;                # Phytoplankton N-biomass (ugN L-1)
Am = 70;                # Ammonium-N (ugN L-1)
Zoo = 0.1;              # Zooplankton N-biomass (ugN L-1)
Corpse = 0;             # Zooplankton corpse (ugN L-1)
sysN	= Am + Phy + Zoo + Corpse; # System N-balance (ugN L-1)
# Initial conditions array
x0 = [Am, Phy, Zoo, sysN, Corpse];

# Simulate
y = solver(@func_closure, tspan, stepsize, x0);

# Plot the results
h = figure;

subplot(2, 2, 1);
plot(tspan, y(:, 4), 'r', tspan, y(:, 1), 'g', tspan, y(:, 2), 'b', tspan, y(:, 3), 'k', tspan, y(:, 5), 'm');
set(gca,'FontSize',12);
xlabel('Time (d)', 'FontSize', 12);
ylabel('\mugN L^{-1}', 'FontSize', 12);
hleg = legend('sysN', 'Am', 'Phy', 'Zoo', 'Corpse');
set(hleg, 'FontSize', 8);

subplot(2, 2, 2);
plot(tspan(2:end), u_Phy', 'r', tspan(2:end), u_Zoo', 'g');
set(gca,'FontSize',12);
xlabel('Time (d)', 'FontSize', 12);
ylabel('d^{-1}', 'FontSize', 12);
hleg = legend('u\_Phy', 'u\_Zoo');
set(hleg, 'FontSize', 8);

subplot(2, 2, 3);
plot(y(:, 1), y(:, 2), 'r');
set(gca,'FontSize',12);
xlabel('Am', 'FontSize', 12);
ylabel('Phy', 'FontSize', 12);

subplot(2, 2, 4);
plot(y(:, 2), y(:, 3), 'r');
set(gca,'FontSize',12);
xlabel('Phy', 'FontSize', 12);
ylabel('Zoo', 'FontSize', 12);

print(h, 'Chapter-11-Closure.png', '-dpng', '-color');