## GNU Octave ports of the models in "Dynamic Ecology - an introduction to
## the art of simulating trophic dynamics" by Flynn, K. (2018).
## Copyright (C) 2020  Ekin Akoglu and Kevin J. Flynn
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <https://www.gnu.org/licenses/>.

clear;

global growth
global death

# Simulation time frame
t0 = 0;      # start time
tfinal = 20; # end time
stepsize = 0.0625;
tspan = (t0:stepsize:tfinal); # time span

# Preallocate global arrays for speed
growth = zeros(1, length(tspan)-1);
death = zeros(1, length(tspan)-1);

# Initial conditions
Pop = 1; # nos Population size
x0 = Pop;

# Simulate
y = solver(@func_logistic, tspan, stepsize, x0);

# Plot the results
h = figure;

subplot(1, 2, 1);
plot(tspan, y, 'r');
set(gca,'FontSize',12);
xlabel('Time (d)', 'FontSize', 12);
ylabel('Pop', 'FontSize', 12);

subplot(1, 2, 2);
plot(tspan(2:end), growth', 'r', tspan(2:end), death', 'g');
set(gca,'FontSize',12);
xlabel('Time (d)', 'FontSize', 12);
ylabel('nos d^{-1}', 'FontSize', 12);
hleg = legend('growth', 'death', 'location', 'southeast');
set(hleg, 'FontSize', 8);

print(h, 'Chapter-6-Logistic-Model.png', '-dpng', '-color');