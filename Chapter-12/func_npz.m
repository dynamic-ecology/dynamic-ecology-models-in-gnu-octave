## GNU Octave ports of the models in "Dynamic Ecology - an introduction to
## the art of simulating trophic dynamics" by Flynn, K. (2018).
## Copyright (C) 2020  Ekin Akoglu and Kevin J. Flynn
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <https://www.gnu.org/licenses/>.

function xdot = func_npz(t, x)

global tspan
global mix_dep
global H
global tot_mix
global PSqz
global Nu
global psu
global G
global Zu
global AP
global Rate_1
global Rate_2

# Physical parameters
dif_mix = 0.18796;  # Diffusive mixing (m d-1)
atmos_clar = 0.38;  # Corrects for atmospheric clarity (varies with lat, long  & JD) (dl)
con_fact = 4.57;    # Converts W m-2 to PAR umol m-2 s-1 for cloud-less sky with sun (dl)
lat = 47;           # Latitude;
solar_const = 1368; # Solar constant irradiance (W m-2 = J/m2/s); maximum irradiance to Earth from the sun (W m-2)
attco_W = 0.032323; # Absorbance coefficient for growth medium (water) (m-1)
abco_Chl = 0.02;    # Light absorbance coefficient for chlorophyll (m2 (mgChl)-1)

# Nutrient parameters
ext_NO3 = 7.25;     # Nitrate concentration below mixed layer (mmolN m-3)
remin_frac = 0.167; # Fraction remineralised (dl)

# Phytoplankton parameters
alpha = 7.00E-06;     # Slope of Chl-specific PE curve 	(m2 g-1 chl.a)*(gC umol-1 photon)
ChlC = 0.06;          # Mass ratio content of chlorophyll:C in the phytoplankton (gChl (gC)-1)
inflow_Phy = 10 * 14; # Concentration of incoming phytoplankton biomass (mgN m-3)
phy_k = 0.5;          # Half saturation constant for Nu (mmolN m-3)
Pmax = 0.5;           # Phytoplankton maximum N-specific growth rate (gN (gN)-1 d-1)
NC = 0.15;            # Mass ratio content of N-biomass:C in the phytoplankton (gN (gC)-1)
P_mort = 0.05;        # Mortality rate for phytoplankton (d-1)

# Zooplankton parameters
AE = 0.75;         # Assimilation efficiency (dl)
ex_rate = 0.05;    # Excretion rate (d-1)
G_max = 0.7;       # Maximum N-specific grazing rate of zooplankton on phytoplankton (d-1)
K_pred = 0.761354; # Half saturation constant for predat,on (mmolN m-3)
Z_mort = 0.564669; # Closure constant (dl);

## Auxiliaries
# Selection of only positive values of H
if H(t - 1) > 0
  H_plus = H(t - 1);
else
  H_plus = 0;
endif

# Total mixing across the ergocline (d-1)
tot_mix(t - 1) = (dif_mix + H_plus) / mix_dep(t - 1);

# Current time as fraction of day (dl)
frac_day = tspan(t - 1) - floor(tspan(t - 1));

# Julian day; note the 10d offset (starting the year on 22nd of December) (d)
JD = 365 * (((tspan(t - 1) + 10) / 365) - floor((tspan(t - 1) + 10) / 365));

# Current time as fraction of day in hours (hrs)
t_24 = 24 * frac_day;

# Degree of hour angle away from noon (default 12:00) (dl)
deg_hr = abs(12 - t_24) * 15;

# Hour angle radians (rad)
r_hr = deg_hr * pi / 180;

# Latitude in radians (rad)
r_lat = lat * pi / 180;

# Solar declination angle (rad)
sol_deca = 23.45 * sin(2 * pi * (284 + JD) * 0.00274) * pi / 180;

# Cosine of zenith angle (dl)
coszen = max(sin(r_lat) * sin(sol_deca) + cos(r_lat) * cos(sol_deca) * cos(r_hr), 0);

# Angle the sun makes with the vertical (solar zenith angle) (rad)
theta1 = acos(coszen);

# Angle the sun makes with the vertical (solar zenith angle) (degrees)
deg_1 = theta1 * deg2rad(1.0);

# Proportion of light incident with the water surface that is just under the surface, accounting for reflectance (dl)
E_enter =	1 - (1.15e-06 * deg_1^3 - 69.1340e-06 * deg_1^2 + 0.001 * deg_1 + 0.0187);

# Earth radius vector
r_vec	=	1 / (1 + 0.033 * cos(2 * pi * JD * 0.00274))^0.5;

# Value of coszen at noon (hence COS(0) at end of definition) (dl)
Noon_coszen	=	max(sin(r_lat) * sin(sol_deca) + cos(r_lat) * cos(sol_deca) * cos(0), 0);

# Maximum irradiance (at noon) on this Julian date (W m-2)
Noon_Wm2 =	solar_const / r_vec / r_vec * Noon_coszen;

if coszen > 0
  mult1 = 1;
else
  mult1 = 0;
endif
# Irradiance at given hour and day; W m-2 [W = J s-1; i.e. J/m2/s] (W m-2)
Wm2	=	solar_const / r_vec / r_vec * coszen * mult1;

# Light actually entering water (just under surface), accounting for reflectance (W m-2)
Wm2_enter	=	Wm2 * E_enter * atmos_clar;

# Photon m-2 s-1 PFD just under surface (umol)
nat_PFD	=	Wm2_enter * con_fact;

# Nitrate input and nutrient-N output (mmolN m-3 d-1)
N_mix = (ext_NO3 - x(1)) * tot_mix(t - 1);

# Phytoplankton-N specific coefficient for light absorbance (m2 (mgN)-1)
abco_PhyN = abco_Chl * ChlC / NC;

# Specific slope of PE curve ((m2)*(umol-1 photon))
alpha_u = alpha * ChlC;

# Attenuation coefficient to phytoplankton N-biomass (m-1)
attco_Phy = abco_PhyN * x(2) * 14;

# Total attenuation (dl)
att_tot = mix_dep(t - 1) * (attco_W + attco_Phy);

# Negative exponent of total attenuation (dl)
exatt = exp(-att_tot);

# Index of N-limitation (dl)
Nu(t - 1) = x(1) / (x(1) + phy_k);

# Maximum photosynthetic rate down-regulated in consequence of nutrient stress (d-1)
PSqmax = Pmax * Nu(t - 1);

# Intermediate in depth-integrated photosynthesis rate (d)
pytq = (alpha_u * nat_PFD * 24 * 60 * 60) / PSqmax;

# Phytoplankton N-specific growth rate (d-1)
PSqz(t - 1) = PSqmax * (log(pytq + sqrt(1 + pytq^2)) - log(pytq * exatt + sqrt(1 + (pytq * exatt)^2))) / att_tot;

# Relative photosynthetic rate (dl)
psu(t - 1) = PSqz(t - 1) / Pmax;

# N-assimilation by phytoplankton (mmolN m-3 d-1)
N_ass = x(2) * PSqz(t - 1);

# Loss of phytoplankton by death (mmolN m-3 d-1)
P_death = x(2) * P_mort;

# Removal of phytoplankton by mixing (mmolN m-3 d-1)
P_mix = x(2) * tot_mix(t - 1);

# Closure term on zooplankton (mmolN m-3 d-1)
Z_death = Z_mort * x(3)^2;

# Remineralisation of zooplankton corpses to nutrient-N within mixed layer (mmolN m-3 d-1)
corpse_remin = Z_death * remin_frac;

# Grazing rate by zooplankton on phytoplankton (mmolN m-3 d-1)
pred = x(3) * G_max * x(2) / (x(2) + K_pred);

# N-specific grazing rate (d-1)
G(t - 1) = G_max * (x(2) / (x(2) + K_pred));

# N-specific zooplankton growth rate
Zu(t - 1) = (G_max * (x(2) / (x(2) + K_pred)) * AE) - ex_rate;

# Defecation by zooplankton (mmolN m-3 d-1)
defec = (1 - AE) * pred;

# Regeneration of N by zooplankton (mmolN m-3 d-1)
excret = x(3) * ex_rate;

# Remineralisation of faecal pellets (mmolN m-3 d-1)
pellet_remin = defec * remin_frac;

# Removal of zooplankton by mixing (mmolN m-3 d-1)
Z_mix = x(3) * tot_mix(t - 1);

# Depth integrated areal primary production (mmolN m-2 d-1)
AP(t - 1) = N_ass * mix_dep(t - 1);

# Intermediate calc
Rate_1(t - 1) =	AP(t - 1);

# Intermediate calc; to average over 1 time unit (day)
if t < 34
  Rate_2(t - 1) = 0;
else
  Rate_2(t - 1) =	Rate_1(t - 33);
endif

## State equations
# Inorganic-N
xdot(1, 1) = excret + pellet_remin + corpse_remin + N_mix - N_ass;

# Phytoplankton
xdot(1, 2) = N_ass - pred - P_mix - P_death;

# Zooplankton
xdot(1, 3) = pred - excret - defec - Z_death;

# Corpse
xdot(1, 4) = Z_death - corpse_remin;

# Pellets
xdot(1, 5) = defec - pellet_remin;

# cum_prod
xdot(1, 6) = Rate_1(t - 1);

# DAY_avg_AP
xdot(1, 7) = Rate_1(t - 1) - Rate_2(t - 1);

endfunction

