## GNU Octave ports of the models in "Dynamic Ecology - an introduction to
## the art of simulating trophic dynamics" by Flynn, K. (2018).
## Copyright (C) 2020  Ekin Akoglu and Kevin J. Flynn
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <https://www.gnu.org/licenses/>.

clear;

global att_tot
global Nu
global PSrel
global PSqz
global attco_Phy
global attco_W

# Simulation time frame
t0 = 0;      # start time
tfinal = 20; # end time
stepsize = 0.0625;
tspan = (t0:stepsize:tfinal); # time span

# Preallocate global arrays for speed
attot = zeros(1, length(tspan)-1);
Nu = zeros(1, length(tspan)-1);
PSrel = zeros(1, length(tspan)-1);
PSqz = zeros(1, length(tspan)-1);
attco_Phy = zeros(1, length(tspan)-1);

# Initial conditions
Am = 14 * 20;    # Ammonium-N concentration (ugN L-1)
Phy = 1;         # Phytoplankton biomass-N concentration (ugN L-1)
sysN = Am + Phy; # System N-balance (ugN L-1)
# Initial conditions array
x0 = [Am Phy sysN];

# Simulate
y = solver(@func_light_N_limited, tspan, stepsize, x0);

# Plot the results
h = figure;

subplot(2, 2, 1);
plot(tspan, y(:,1), 'r', tspan, y(:,2), 'g', tspan, y(:,3), 'b');
set(gca,'FontSize',12);
xlabel('Time (d)', 'FontSize', 12);
ylabel('\mugN L^{-1}', 'FontSize', 12);
hleg = legend('Am', 'Phy', 'sysN', 'location', 'east');
set(hleg, 'FontSize', 8);

subplot(2, 2, 2);
plot(tspan(2:end), att_tot', 'r');
set(gca,'FontSize',12);
xlabel('Time (d)', 'FontSize', 12);
ylabel('att\_tot', 'FontSize', 12);

subplot(2, 2, 3);
plot(tspan(2:end), Nu, 'r', tspan(2:end), PSrel', 'g', tspan(2:end), PSqz, 'b');
set(gca,'FontSize',12);
xlabel('Time (d)', 'FontSize', 12);
hleg = legend('Nu', 'PSrel', 'PSqz', 'location', 'east');
set(hleg, 'FontSize', 8);

subplot(2, 2, 4);
plot(tspan(2:end), repmat(attco_W, 1, length(tspan)-1), 'r', tspan(2:end), attco_Phy, 'g');
set(gca,'FontSize',12);
xlabel('Time (d)', 'FontSize', 12);
hleg = legend('attco\_W', 'attco\_Phy', 'location', 'east');
set(hleg, 'FontSize', 8);

print(h, 'Chapter-8-Light-N-limited.png', '-dpng', '-color');