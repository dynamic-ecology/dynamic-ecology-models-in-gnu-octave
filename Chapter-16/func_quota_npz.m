## GNU Octave ports of the models in "Dynamic Ecology - an introduction to
## the art of simulating trophic dynamics" by Flynn, K. (2018).
## Copyright (C) 2020  Ekin Akoglu and Kevin J. Flynn
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <https://www.gnu.org/licenses/>.

function xdot = func_quota_npz(t, x)

global NC_Phy
global NC_Zoo
global u_Phy
global u_Zoo
global uN_Phy
global NCu_Phy
global AEmin
global AEmax
global AEqual
global AEquan
global AEC_Zoo
global ingC_Zoo
global ingCmax_Zoo
global AEC
global AEN
global GGEC
global GGEN
global palat_Phy
global NCPhy_Zoo
global Stoich_con
global XSC

# Dilution
relDil = 0.05; # Dilution rate relative to umax_Phy (dl)

# Phytoplankton parameters
ktAM_Phy	=	14;   # Half saturation constant for ammonium transport (ugN L-1)
umax_Phy = 0.693; # Maximum C-specific growth rate (gC (gC)-1 d-1)
NCmin_Phy = 0.05; # Minimum NC_Phy (gN (gC)-1)
NCmax_Phy = 0.15; # Maximum NC_Phy (gN (gC)-1)
kQN_Phy = 10;     # KQ for N-quota (dl)

# Zooplankton parameters
AEmax = 0.6;      # Maximum AE for N (dl)
AEmin = 0.2;      # Maximum AE for N (dl)
BR_Zoo = 0.1;     # Basal respiration rate as a proportion of umax_Zoo (dl)
kAE = 1.00e+03;   # Constant for control of AE in response to prey quality (dl)
kCPhy_Zoo =140;   # Half saturation of zooplankton predation on phytoplankton (ugC L-1)
kGTT = 100;       # Curve control for density dependant inefficiency (ug C L-1)
minAE_mult = 1;   # Minimum AEC scalar for density dependant inefficiency (dl)
NC_Zoo = 0.2;     # N:C of zooplankton (gN gC-1)
SDA = 0.3;        # Specific dynamic action (dl)
thresC_Phy = 0.014; # Threshold for predation upon phytoplankton (ugC L-1)
tox_Phy = 0.6;    # Toxicity scalar (dl)
umax_Zoo = 0.693; # Zooplankton maximum growth rate (d-1)

# Voided materials parameters
NCmax = 0.3;      # Maximum mass ratio of N:C which could be attained in the organic form (gN (gC)-1)

## Auxiliaries
# Dilution rate (d-1)
dil = relDil * umax_Phy;

# Nutrient exchange (ugN L-1 d-1)
in_out_Am = dil * (140 - x(1));

# Washout of N_Phy (ugN L-1 d-1)
outN_Phy = x(2) * dil;

# Washout of C_Phy (ugC L-1 d-1)
outC_Phy = x(3) * dil;

# Phytoplankton N:C quota (gN (g C)-1)
NC_Phy(t - 1) = x(2) / x(3);

# Quotient for N status (dl)
NCu_Phy(t - 1) = ((1 + kQN_Phy) * (NC_Phy(t - 1) - NCmin_Phy)) / ((NC_Phy(t - 1) - NCmin_Phy) + kQN_Phy * (NCmax_Phy - NCmin_Phy));

# C-specific growth rate controlled by N:C quota (gC (gC)-1 d-1)
u_Phy(t - 1) = umax_Phy * NCu_Phy(t - 1);

# Maximum C-specific N transport rate (gN (gC)-1 d-1)
TNmax_Phy = umax_Phy * NCmax_Phy;

# Phytoplankton C-specific N transport rate (gN (gC)-1 d-1)
NCt_Phy = TNmax_Phy * x(1) / (x(1) + ktAM_Phy);

# N-specific growth rate (gN (gN)-1 d-1)
uN_Phy(t - 1) = NCt_Phy / NC_Phy(t -1);

# Phytoplankton population uptake of ammonium-N (ugN L-1 d-1)
Nup_Phy = x(3) * NCt_Phy;

# Growth rate in phytoplankton-C (ugC L-1 d-1)
groC_Phy = x(3) * u_Phy(t - 1);

# Ratio of NC in prey compared to predator (dl)
NCPhy_Zoo(t - 1) = NC_Phy(t -1) / NC_Zoo;

# Selection of release of N related to difference in food to consumer N:C (dl)
Stoich_con(t - 1) = min(NCPhy_Zoo(t - 1), 1);

# AEC scalar for density dependant inefficiency (dl)
AEquan(t - 1) = (1 - minAE_mult) * (1 - x(3) / (x(3) + kGTT)) + minAE_mult;

# Efficiency parameter for assimilation (dl)
AEqual(t - 1)  = AEmin + (AEmax - AEmin) * Stoich_con(t - 1) / (Stoich_con(t - 1) + kAE) * (1 + kAE);

# Operational AE for C (dl)
AEC_Zoo(t - 1) = Stoich_con(t - 1) * AEqual(t - 1)  * AEquan(t - 1);

# Zooplankton basal respiration rate (d-1)
BR = umax_Zoo * BR_Zoo;

# Maximum ingestion rate by zooplankton (gC (gC)-1 d-1)
ingCmax_Zoo(t -1) = (umax_Zoo * (1 + SDA) + BR) / AEC_Zoo(t - 1) ;

# Palatability index (0 not palatable) (dl)
palat_Phy(t - 1) = (NCu_Phy(t - 1) + 1.0e-6)^tox_Phy;

# Ingestion rate pf prey into zooplankton (gC (gC)-1 d-1)
if x(3) > thresC_Phy
  ingC_Zoo(t -1) = palat_Phy(t - 1) * ingCmax_Zoo(t -1) * (x(3) - thresC_Phy) / (x(3) - thresC_Phy + kCPhy_Zoo);
else
  ingC_Zoo(t -1) = 0;
endif

# C available for support of respiration (gC (gC)-1 d-1)
if Stoich_con(t - 1) < 1
  XSC(t - 1) = AEqual(t - 1)  * ingC_Zoo(t -1) * (1 - Stoich_con(t - 1));
else
  XSC(t - 1) = 0;
endif

# Basal respiration that is met bu respiration of excess C in diet (gC (gC)-1 d-1)
if BR <= XSC(t - 1)
  BRi = BR;
else
  BRi = XSC(t - 1);
endif

# Balance of basal respiration that cannot be met from dietary excees C (gC (gC)-1 d-1)
BRb = BR - BRi;

# Grazing upon phytoplankton population (ugC L-1 d-1)
grazC_Phy = x(4) * ingC_Zoo(t -1);

# Grazing upon phytoplankton population in terms of N (ugN L-1 d-1)
grazN_Phy = x(4) * ingC_Zoo(t -1) * NC_Phy(t -1);

# Assimilation rate into zooplankton (gC (gC)-1 d-1)
assC_Zoo = AEC_Zoo(t - 1)  * ingC_Zoo(t -1);

# Assimilation of C into zooplankton population biomass (ugC L-1)
assC = x(4) * assC_Zoo;

# Zooplankton respiration rate (gC (gC)-1 d-1)
resC_Zoo = BRb + assC_Zoo * SDA; 

# Zooplankton population respiration (ugC L-1 d-1)
respC = x(4) * resC_Zoo;

# Zooplankton growth rate (gC (gC)-1 d-1)
u_Zoo(t - 1) = assC_Zoo - resC_Zoo;

# Amount of N initially in the organic form to be voided to maintain constant predator N:C (gN (gC)-1 d-1)
XSassN = ingC_Zoo(t -1) * NC_Phy(t -1) - assC_Zoo * NC_Zoo;

# AE in terms of C (dl)
AEC(t - 1) = assC_Zoo / ingC_Zoo(t -1);

# AR in terms of N (dl)
AEN(t - 1) = (assC_Zoo * NC_Zoo) / (ingC_Zoo(t -1) * NC_Phy(t -1));

# Voiding of C by zooplankton (gC (gC)-1 d-1)
voidC_Zoo = ingC_Zoo(t -1) - assC_Zoo - BRi;

# Population rate of C voiding (ugC L-1 d-1)
voidC = x(4) * voidC_Zoo;

# Voiding of N by zooplankton (gN (gC)-1 d-1)
if (XSassN / voidC_Zoo) > NCmax
  voidN_Zoo = voidC_Zoo * NCmax;
else
  voidN_Zoo = XSassN;
endif

# Population rate of N voiding (ugN L-1 d-1)
voidN = x(4) * voidN_Zoo;

# Zooplankton ammonium regeneration (gN (gC)-1 d-1)
DINr = resC_Zoo * NC_Zoo + XSassN - voidN_Zoo;

# GGE in terms of C (dl)
GGEC(t - 1) = (ingC_Zoo(t -1) - voidC_Zoo - resC_Zoo - BRi) / ingC_Zoo(t -1);

# GGE in terms of N (dl)
GGEN(t - 1) = (ingC_Zoo(t -1) * NC_Phy(t -1) - voidN_Zoo - DINr) / (ingC_Zoo(t -1) * NC_Phy(t -1));

# Zooplankton population regeneration of ammonium (ugN L-1 d-1)
reg_Am = x(4) * DINr;

# Washout of voided C (ugC L-1 d-1)
out_VOC = x(6) * dil;

# Washout of voided N (ugN L-1 d-1)
out_VON = x(5) * dil;

# Washout of zooplankton biomass (ugC L-1 d-1)
outC_Zoo = x(4) * dil;

## State equations
# Ammonium
xdot(1, 1) = in_out_Am + reg_Am - Nup_Phy;

# Phytoplankton-N
xdot(1, 2) = Nup_Phy - grazN_Phy - outN_Phy;

# Phytoplankton-C
xdot(1, 3) = groC_Phy - grazC_Phy - outC_Phy;

# Zooplankton-C
xdot(1, 4) = assC - respC - outC_Zoo;

# VON
xdot(1, 5) = voidN - out_VON;

# VOC
xdot(1, 6) = voidC - out_VOC;

# System
xdot(1, 7) = xdot(1, 1) + xdot(1, 2) + xdot(1, 4) * NC_Zoo + xdot(1, 5);

endfunction

