## GNU Octave ports of the models in "Dynamic Ecology - an introduction to
## the art of simulating trophic dynamics" by Flynn, K. (2018).
## Copyright (C) 2020  Ekin Akoglu and Kevin J. Flynn
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <https://www.gnu.org/licenses/>.

function xdot = func_logistic(t, x)

global growth
global death

# Phytoplankton parameters
K	=	100;   # Carrying capacity (maximum Pop, nos)
r = 0.693; # Populations-specific growth rate (nos nos-1 d-1)

## Auxiliaries
growth(t - 1) =	r * x;      # Growth rate (nos d-1)
death(t - 1) = r * x^2 / K; # Death rate (nos d-1)

## State equations
xdot = growth(t - 1) - death(t - 1);

endfunction

