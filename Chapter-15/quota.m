## GNU Octave ports of the models in "Dynamic Ecology - an introduction to
## the art of simulating trophic dynamics" by Flynn, K. (2018).
## Copyright (C) 2020  Ekin Akoglu and Kevin J. Flynn
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <https://www.gnu.org/licenses/>.

clear;

global NC_Phy
global NCu_Phy
global u_Phy
global uN_Phy

# Simulation time frame
t0 = 0;      # start time
tfinal = 20; # end time
stepsize = 0.03125;
tspan = (t0:stepsize:tfinal); # time span

# Preallocate global arrays for speed
NC_Phy = zeros(1, length(tspan)-1);
NCu_Phy = zeros(1, length(tspan)-1);
u_Phy = zeros(1, length(tspan)-1);
uN_Phy = zeros(1, length(tspan)-1);

# Initial conditions
Am = 14 * 10;         # Ammonium-N (ugN -L-1)
C_Phy = 12;           # Phytoplankton-C (ugC -L-1) 
N_Phy = C_Phy * 0.05; # Phytoplankton-N (ugN -L-1)
sysN	= Am + N_Phy;   # System N (ugN L-1)
# Initial conditions arrayfun
x0 = [Am, N_Phy, C_Phy, sysN];

# Simulate
y = solver(@func_quota, tspan, stepsize, x0);

# Plot the results
h = figure;

subplot(3, 2, 1)
plot(tspan, y(:, 4), 'r', tspan, y(:, 1), 'g', tspan, y(:, 2), 'b');
set(gca,'FontSize',12);
xlabel('Time (d)', 'FontSize', 12);
ylabel('\mugN L^{-1}', 'FontSize', 12);
legend('sysN', 'Am', 'N\_Phy');

subplot(3, 2, 2)
plot(tspan, y(:, 3), 'r');
set(gca,'FontSize',12);
xlabel('Time (d)', 'FontSize', 12);
ylabel('\mugC L^{-1}', 'FontSize', 12);
legend('C\_Phy', 'location', 'southeast');

subplot(3, 2, 3)
plot(NC_Phy, NCu_Phy, 'r');
set(gca,'FontSize',12);
xlabel('NC\_Phy', 'FontSize', 12);
ylabel('NCu\_Phy', 'FontSize', 12);

subplot(3, 2, 4)
plot(tspan(2:end), u_Phy, 'r', tspan(2:end), uN_Phy, 'g', tspan(2:end), NCu_Phy, 'b');
set(gca,'FontSize',12);
xlabel('Time (d)', 'FontSize', 12);
legend('u\_Phy', 'uN\_Phy', 'NCu\_Phy');

subplot(3, 2, 5)
plot(y(2:end, 1), u_Phy, 'r');
set(gca,'FontSize',12);
xlabel('Am', 'FontSize', 12);
ylabel('u\_Phy', 'FontSize', 12);

subplot(3, 2, 6)
plot(y(2:end, 1), uN_Phy, 'r');
set(gca,'FontSize',12);
xlabel('Am', 'FontSize', 12);
ylabel('uN\_Phy', 'FontSize', 12);

print(h, 'Chapter-15-Quota.png', '-dpng', '-color');