## GNU Octave ports of the models in "Dynamic Ecology - an introduction to
## the art of simulating trophic dynamics" by Flynn, K. (2018).
## Copyright (C) 2020  Ekin Akoglu and Kevin J. Flynn
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Euler explicit time forward iteration

function y = solver(fun, tspan, stepsize, x0)
  
  y = zeros(size(tspan, 2), size(x0, 2));
  y(1, :) = x0;
  step = 1;
  
  for t = 2:size(tspan, 2)
    
    xdot = fun(t, y(step, :));
    y(t, :) = y(step, :) + stepsize * xdot(1, :);
    
    step = step + 1;
  endfor
  
  
endfunction
