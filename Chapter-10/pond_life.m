## GNU Octave ports of the models in "Dynamic Ecology - an introduction to
## the art of simulating trophic dynamics" by Flynn, K. (2018).
## Copyright (C) 2020  Ekin Akoglu and Kevin J. Flynn
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <https://www.gnu.org/licenses/>.

clear;

global tspan
global g_Light
global g_W
global gRH
global gT_air
global spillout
global inflow_Pond
global seep_Pond
global evap_Pond
global z_Pond
global z_spill
global zOp_spill
global DIN
global umax_Phy
global umax_Zoo
global u_Phy
global u_Zoo

data = data = dlmread('PowerSim.csv', ",", [1, 0, 1921, 4]);

gRH = data(:, 2);      # Relative humidity data (%)
g_W = data(:, 3);      # Wind input data (m s-1)
gT_air = data(:, 4);   # Air temperature data (Celsius)
g_Light = data(:, 5);  # Light input data (W m-2)

# Simulation time frame
t0 = 0;      # start time
tfinal = 30; # end time
stepsize = 0.015625;
tspan = (t0:stepsize:tfinal); # time span

# Preallocate global arrays for speed
spillout = zeros(1, length(tspan)-1);
inflow_Pond = zeros(1, length(tspan)-1);
seep_Pond = zeros(1, length(tspan)-1);
evap_Pond = zeros(1, length(tspan)-1);
z_Pond = zeros(1, length(tspan)-1);
z_spill = zeros(1, length(tspan)-1);
zOp_spill = zeros(1, length(tspan)-1);
DIN = zeros(1, length(tspan)-1);
umax_Phy = zeros(1, length(tspan)-1);
umax_Zoo = zeros(1, length(tspan)-1);
u_Phy = zeros(1, length(tspan)-1);
u_Zoo = zeros(1, length(tspan)-1);

# Initial conditions
N_Phy = 4200;   # Phytoplankton biomass (mgN)
N_Pond = 42000; # Pond nutrient-N content (mgN)
N_Zoo = 4200;   # Zooplankton N-biomass (mgN)
T_Pond = 10;    # Temperature of pond water (Celsius)
V_Pond = 30;    # Pond volume (m3) 
# Initial conditions array
x0 = [N_Phy N_Pond N_Zoo T_Pond V_Pond];

# Simulate
y = solver(@func_pond_life, tspan, stepsize, x0);

# Plot the results
h = figure;

subplot(4, 2, 1);
plot(tspan(2:end), spillout, 'r', tspan(2:end), inflow_Pond, 'g', tspan(2:end), seep_Pond, 'b', tspan(2:end), evap_Pond, 'm');
set(gca,'FontSize',12);
xlabel('Time (d)', 'FontSize', 12);
ylabel('m^{3} d^{-1}', 'FontSize', 12);
hleg = legend('spillout', 'inflow\_Pond', 'seep\_Pond', 'evap\_Pond', 'location', 'eastoutside');
set(hleg, 'FontSize', 8);
ylim([0 20]);

subplot(4, 2, 2);
plot(tspan(2:end), z_Pond, 'r', tspan(2:end), z_spill, 'g', tspan(2:end), zOp_spill, 'b');
set(gca,'FontSize',12);
xlabel('Time (d)', 'FontSize', 12);
ylabel('depth (m)', 'FontSize', 12);
hleg = legend('z\_Pond', 'z\_spill', 'zOp\_spill', 'location', 'eastoutside');
set(hleg, 'FontSize', 8);
ylim([0 0.4]);

subplot(4, 2, 3);
plot(tspan, y(:, 5), 'r');
set(gca,'FontSize',12);
xlabel('Time (d)', 'FontSize', 12);
ylabel('V\_Pond (m^{3})', 'FontSize', 12);
ylim([0 35]);
set(gca, 'YTick', 0:10:30);

subplot(4, 2, 4);
plot(tspan, y(:, 4), 'r');
set(gca,'FontSize',12);
xlabel('Time (d)', 'FontSize', 12);
ylabel('T\_Pond (C)', 'FontSize', 12);
ylim([5 20]);
set(gca, 'YTick', 5:5:20);

subplot(4, 2, 5);
plot(tspan, y(:, 2), 'r', tspan, y(:, 1), 'g', tspan, y(:, 3), 'b');
set(gca,'FontSize',12);
xlabel('Time (d)', 'FontSize', 12);
ylabel('mgN', 'FontSize', 12);
hleg = legend('N\_Pond', 'N\_Phy', 'N\_Zoo', 'location', 'eastoutside');
set(hleg, 'FontSize', 8);

subplot(4, 2, 6);
plot(tspan, (y(:, 2) ./ y(:, 5)) ./ 14, 'r', tspan, (y(:, 1) ./ y(:, 5)) ./ 14, 'g', tspan, (y(:, 3) ./ y(:, 5)) ./ 14, 'b');
set(gca,'FontSize',12);
xlabel('Time (d)', 'FontSize', 12);
ylabel('mgN m^{-3}', 'FontSize', 12);
hleg = legend('DIN', 'Phy', 'Zoo', 'location', 'eastoutside');
set(hleg, 'FontSize', 8);

subplot(4, 2, 7);
plot(tspan(2:end), umax_Phy, 'r', tspan(2:end), umax_Zoo, 'g');
set(gca,'FontSize',12);
xlabel('Time (d)', 'FontSize', 12);
ylabel('d^{-1}', 'FontSize', 12);
hleg = legend('umax\_Phy', 'umax\_Zoo', 'location', 'eastoutside');
set(hleg, 'FontSize', 8);

subplot(4, 2, 8);
plot(tspan(2:end), u_Phy, 'b', tspan(2:end), u_Zoo, 'g');
set(gca,'FontSize',12);
xlabel('Time (d)', 'FontSize', 12);
ylabel('d^{-1}', 'FontSize', 12);
hleg = legend('u\_Phy', 'u\_Zoo', 'location', 'eastoutside');
set(hleg, 'FontSize', 8);
ylim([-0.5 1]);

set(gcf, 'PaperPosition', [0.25000 2.50000 9.00000 12.00000]);
print(h, 'Chapter-10-PondLife.png', '-dpng', '-color');