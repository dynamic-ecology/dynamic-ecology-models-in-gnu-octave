## GNU Octave ports of the models in "Dynamic Ecology - an introduction to
## the art of simulating trophic dynamics" by Flynn, K. (2018).
## Copyright (C) 2020  Ekin Akoglu and Kevin J. Flynn
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <https://www.gnu.org/licenses/>.

clear;

global NC_Phy1
global NC_Phy2
global u_Phy1
global u_Phy2
global u_Zoo
global uN_Phy1
global NCu_Phy1
global NCu_Phy2
global AEmin
global AEmax
global AEqual
global AEquan
global AEC_Zoo
global ingC_Zoo
global ingCmax_Zoo
global AEC
global AEN
global GGEC
global GGEN
global palat_Phy1
global palat_Phy2
global Enc_Phy1
global Enc_Phy2
global ingPhy1C
global ingPhy2C
global PR_Phy1
global PR_Phy2
global CRC_Phy1
global CRC_Phy2
global NCPhy_Zoo
global Stoich_con
global XSC

# Simulation time frame
t0 = 0;      # start time
tfinal = 200; # end time
stepsize = 0.015625;
tspan = (t0:stepsize:tfinal); # time span

# Preallocate global arrays for speed
NC_Phy1 = zeros(1, length(tspan)-1);
NC_Phy2 = zeros(1, length(tspan)-1);
u_Phy1 = zeros(1, length(tspan)-1);
u_Phy2 = zeros(1, length(tspan)-1);
u_Zoo = zeros(1, length(tspan)-1);
uN_Phy1 = zeros(1, length(tspan)-1);
NCu_Phy1 = zeros(1, length(tspan)-1);
NCu_Phy2 = zeros(1, length(tspan)-1);
AEC_Zoo = zeros(1, length(tspan)-1);
ingC_Zoo = zeros(1, length(tspan)-1);
ingCmax_Zoo = zeros(1, length(tspan)-1);
AEC = zeros(1, length(tspan)-1);
AEN = zeros(1, length(tspan)-1);
GGEC = zeros(1, length(tspan)-1);
GGEN = zeros(1, length(tspan)-1);
palat_Phy1 = zeros(1, length(tspan)-1);
palat_Phy2 = zeros(1, length(tspan)-1);
Enc_Phy1 = zeros(1, length(tspan)-1);
Enc_Phy2 = zeros(1, length(tspan)-1);
ingPhy1C = zeros(1, length(tspan)-1);
ingPhy2C = zeros(1, length(tspan)-1);
PR_Phy1 = zeros(1, length(tspan)-1);
PR_Phy2 = zeros(1, length(tspan)-1);
CRC_Phy1 = zeros(1, length(tspan)-1);
CRC_Phy2 = zeros(1, length(tspan)-1);
NCPhy_Zoo = zeros(1, length(tspan)-1);
Stoich_con = zeros(1, length(tspan)-1);

# Initial conditions
Am = 280;             # Ammonium-N (ugN L-1)
C_Phy1 = 12;          # Phytoplankton1-C (ugC L-1) 
N_Phy1 = C_Phy1 * 0.07; # Phytoplankton1-N (ugN L-1)
C_Phy2 = 12;          # Phytoplankton2-C (ugC L-1) 
N_Phy2 = C_Phy2 * 0.05; # Phytoplankton2-N (ugN L-1)
C_Zoo = 5;            # Zooplankton C-biomass (ugC L-1)
VON = 0;              # Faecal material-N (ugC L-1)
VOC = 0;              # Faecal material-C (ugN L-1)
sysN	= Am + N_Phy1 + N_Phy2 + C_Zoo * 0.2 + VON; # System N (ugN L-1)
# Initial conditions arrayfun
x0 = [Am, N_Phy1, C_Phy1, N_Phy2, C_Phy2, C_Zoo, VON, VOC, sysN];

# Simulate
y = solver(@func_allometric_quota_npz, tspan, stepsize, x0);

### Plot the results
h = figure;

subplot(2, 2, 1)
plot(tspan, y(:, 2), 'r', tspan, y(:, 4), 'g', tspan, y(:, 6) * 0.2, 'b', tspan, y(:, 7), 'k', tspan, y(:, 1), 'm',  tspan, y(:, 9), 'c');
set(gca,'FontSize',12);
xlabel('Time (d)', 'FontSize', 12);
ylabel('\mugN L^{-1}', 'FontSize', 12);
hleg = legend({'N\_Phy1'; 'N\_Phy2'; 'N\_Zoo'; 'VON'; 'Am'; 'sysN'}, 'location', 'eastoutside');
set(hleg, 'FontSize', 8);

subplot(2, 2, 2)
plot(tspan, y(:, 3), 'r', tspan, y(:, 5), 'g', tspan, y(:, 6), 'b', tspan, y(:, 8), 'k');
set(gca,'FontSize',12);
xlabel('Time (d)', 'FontSize', 12);
ylabel('\mugC L^{-1}', 'FontSize', 12);
hleg = legend('C\_Phy1', 'C\_Phy2', 'C\_Zoo', 'VOC', 'location', 'eastoutside');
set(hleg, 'FontSize', 8);

subplot(2, 2, 3)
plot(tspan(2:end), u_Phy1, 'r', tspan(2:end), u_Phy2, 'g', tspan(2:end), u_Zoo, 'b');
set(gca,'FontSize',12);
xlabel('Time (d)', 'FontSize', 12);
hleg = legend('u\_Phy1', 'u\_Phy2', 'u\_Zoo', 'location', 'eastoutside');
set(hleg, 'FontSize', 8);

subplot(2, 2, 4)
plot(tspan(2:end), NC_Phy1, 'r', tspan(2:end), NC_Phy2, 'g', tspan(2:end), repmat(0.2, 1, length(tspan)-1), 'b', tspan(2:end), (y(2:end, 7) ./ y(2:end, 8)), 'k');
set(gca,'FontSize',12);
xlabel('Time (d)', 'FontSize', 12);
ylabel('N:C', 'FontSize', 12);
hleg = legend('NC\_Phy1', 'NC\_Phy2', 'NC\_Zoo', 'NC\_VO', 'location', 'eastoutside');
set(hleg, 'FontSize', 8);

print(h, 'Chapter-17-Allometric-Quota-NPZ_1.png', '-dpng', '-color');

h2 = figure;

subplot(2, 2, 1)
plot(y(2:end, 1), u_Phy1, 'r');
set(gca,'FontSize',12);
xlabel('Am', 'FontSize', 12);
ylabel('u\_Phy1', 'FontSize', 12);

subplot(2, 2, 2)
plot(y(2:end, 1), uN_Phy1, 'r');
set(gca,'FontSize',12);
xlabel('Am', 'FontSize', 12);
ylabel('uN\_Phy1', 'FontSize', 12);

subplot(2, 2, 3);
plot(tspan(2:end), AEqual, 'r', tspan(2:end), repmat(AEmin, 1, length(tspan)-1), 'g', tspan(2:end), repmat(AEmax, 1, length(tspan)-1), 'b', tspan(2:end), AEC_Zoo, 'k');
set(gca,'FontSize',12);
xlabel('Time (d)', 'FontSize', 12);
hleg = legend('AEqual', 'AEmin', 'AEmax', 'AEC\_Zoo', 'location', 'eastoutside');
set(hleg, 'FontSize', 8);

subplot(2, 2, 4);
plot(tspan(2:end), ingCmax_Zoo, 'r', tspan(2:end), ingC_Zoo, 'g');
set(gca,'FontSize',12);
xlabel('Time (d)', 'FontSize', 12);
hleg = legend('ingCmax\_Zoo', 'ingC\_Zoo', 'location', 'eastoutside');
set(hleg, 'FontSize', 8);

print(h2, 'Chapter-17-Allometric-Quota-NPZ_2.png', '-dpng', '-color');

h3 = figure;

subplot(4, 2, 1);
plot(tspan(2:end), AEC, 'r', tspan(2:end), AEN, 'g');
set(gca,'FontSize',12);
xlabel('Time (d)', 'FontSize', 12);
hleg = legend('AEC', 'AEN', 'location', 'eastoutside');
set(hleg, 'FontSize', 8);

subplot(4, 2, 2);
plot(tspan(2:end), GGEC, 'r', tspan(2:end), GGEN, 'g');
set(gca,'FontSize',12);
xlabel('Time (d)', 'FontSize', 12);
ylim([0 1]);
hleg = legend('GGEC', 'GGEN', 'location', 'eastoutside');
set(hleg, 'FontSize', 8);

subplot(4, 2, 3);
plot(tspan(2:end), Enc_Phy1, 'r', tspan(2:end), Enc_Phy2, 'g');
set(gca,'FontSize',12);
xlabel('Time (d)', 'FontSize', 12);
hleg = legend('Enc\_Phy1', 'Enc\_Phy2', 'location', 'eastoutside');
set(hleg, 'FontSize', 8);

subplot(4, 2, 4);
plot(tspan(2:end), ingPhy1C, 'r', tspan(2:end), ingPhy2C, 'g');
set(gca,'FontSize',12);
xlabel('Time (d)', 'FontSize', 12);
hleg = legend('ingPhy1C', 'ingPhy2C', 'location', 'eastoutside');
set(hleg, 'FontSize', 8);

subplot(4, 2, 5);
plot(tspan(2:end), PR_Phy1, 'r', tspan(2:end), PR_Phy2, 'g');
set(gca,'FontSize',12);
xlabel('Time (d)', 'FontSize', 12);
hleg = legend('PR\_Phy1', 'PR\_Phy2', 'location', 'eastoutside');
set(hleg, 'FontSize', 8);

subplot(4, 2, 6);
plot(tspan(2:end), palat_Phy1, 'r', tspan(2:end), palat_Phy2, 'g');
set(gca,'FontSize',12);
xlabel('Time (d)', 'FontSize', 12);
hleg = legend('palat\_Phy1', 'palat\_Phy2', 'location', 'eastoutside');
set(hleg, 'FontSize', 8);

subplot(4, 2, 7);
plot(tspan(2:end), CRC_Phy1, 'r', tspan(2:end), CRC_Phy2, 'g');
set(gca,'FontSize',12);
xlabel('Time (d)', 'FontSize', 12);
hleg = legend('CRC\_Phy1', 'CRC\_Phy2', 'location', 'eastoutside');
set(hleg, 'FontSize', 8);

subplot(4, 2, 8);
plot(tspan(2:end), XSC, 'r');
set(gca,'FontSize',12);
xlabel('Time (d)', 'FontSize', 12);
ylabel('XSC', 'FontSize', 12);

set(gcf, 'PaperPosition', [0.25000 2.50000 9.00000 12.00000]);
print(h3, 'Chapter-17-Allometric-Quota-NPZ_3.png', '-dpng', '-color');

h4 = figure;

subplot(2, 2, 1);
plot(NC_Phy1, ingCmax_Zoo, 'r');
set(gca,'FontSize',12);
xlabel('NC\_Phy1', 'FontSize', 12);
ylabel('ingCmax\_Zoo', 'FontSize', 12);

subplot(2, 2, 2);
plot(tspan(2:end), NCPhy_Zoo, 'r', tspan(2:end), Stoich_con, 'g');
set(gca,'FontSize',12);
ylabel('Time (d)');
hleg = legend('NCPhy\_Zoo', 'Stoich\_con', 'location', 'eastoutside');
set(hleg, 'FontSize', 8);

subplot(2, 2, 3);
plot(NCu_Phy1, palat_Phy1, 'r');
set(gca,'FontSize',12);
xlabel('palat\_Phy1', 'FontSize', 12);
ylabel('NCu\_Phy1', 'FontSize', 12);

subplot(2, 2, 4);
plot(NCu_Phy1, palat_Phy2, 'r');
set(gca,'FontSize',12);
xlabel('palat\_Phy2', 'FontSize', 12);
ylabel('NCu\_Phy1', 'FontSize', 12);

print(h4, 'Chapter-17-Allometric-Quota-NPZ_4.png', '-dpng', '-color');

h5 = figure;

subplot(3, 2, 1);
plot(tspan(2:end), AEquan, 'r', tspan(2:end), palat_Phy1, 'g');
set(gca,'FontSize',12);
ylim([0 2]);
xlabel('Time (d)', 'FontSize', 12);
ylabel('Index', 'FontSize', 12);
hleg = legend('AEquan', 'palat\_Phy1', 'location', 'eastoutside');
set(hleg, 'FontSize', 8);

subplot(3, 2, 2);
plot(tspan(2:end), u_Phy1, 'r', tspan(2:end), uN_Phy1, 'g', tspan(2:end), NCu_Phy1, 'b');
set(gca,'FontSize',12);
xlabel('Time (d)', 'FontSize', 12);
hleg = legend('u\_Phy1', 'uN\_Phy1', 'NCu\_Phy1', 'location', 'eastoutside');
set(hleg, 'FontSize', 8);

subplot(3, 2, 3);
plot(y(:, 3), y(:, 6), 'r');
set(gca,'FontSize',12);
xlabel('C_Phy1', 'FontSize', 12);
ylabel('C\_Zoo', 'FontSize', 12);

subplot(3, 2, 4);
plot(NC_Phy1, ingC_Zoo, 'r');
set(gca,'FontSize',12);
xlabel('NC\_Phy1', 'FontSize', 12);
ylabel('ingC\_Zoo', 'FontSize', 12);

subplot(3, 2, 5);
plot(tspan(2:end), AEquan, 'r');
set(gca,'FontSize',12);
xlabel('Time (d)', 'FontSize', 12);
ylabel('Aequan', 'FontSize', 12);

print(h5, 'Chapter-17-Allometric-Quota-NPZ_5.png', '-dpng', '-color');