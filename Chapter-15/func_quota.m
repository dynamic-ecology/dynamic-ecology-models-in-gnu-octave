## GNU Octave ports of the models in "Dynamic Ecology - an introduction to
## the art of simulating trophic dynamics" by Flynn, K. (2018).
## Copyright (C) 2020  Ekin Akoglu and Kevin J. Flynn
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <https://www.gnu.org/licenses/>.

function xdot = func_quota(t, x)

global NC_Phy
global NCu_Phy
global u_Phy
global uN_Phy

# Dilution
relDil = 0; # Dilution rate relative to umax_Phy (dl)

# Parameters
ktAM_Phy	=	14;   # Half saturation constant for ammonium transport (ugN L-1)
umax_Phy = 0.693; # Maximum C-specific growth rate (gC (gC)-1 d-1)
NCmin_Phy = 0.05; # Minimum NC_Phy (gN (gC)-1)
NCmax_Phy = 0.15; # Maximum NC_Phy (gN (gC)-1)
kQN_Phy = 10;     # KQ for N-quota (dl)

## Auxiliaries
# Dilution rate (d-1)
dil = relDil * umax_Phy;

# Nutrient exchange (ugN L-1 d-1)
in_out_Am = dil * (140 - x(1));

# Washout of N_Phy (ugN L-1 d-1)
outN_Phy = x(2) * dil;

# Washout of C_Phy (ugC L-1 d-1)
outC_Phy = x(3) * dil;

# Phytoplankton N:C quota (gN (gC)-1)
NC_Phy(t - 1) = x(2) / x(3);

# Quotient for N status (dl)
NCu_Phy(t - 1) = ((1 + kQN_Phy) * (NC_Phy(t - 1) - NCmin_Phy)) / ((NC_Phy(t - 1) - NCmin_Phy) + kQN_Phy * (NCmax_Phy - NCmin_Phy));

# C-specific growth rate controlled by N:C quota (gC (gC)-1 d-1)
u_Phy(t - 1) = umax_Phy * NCu_Phy(t - 1);

# Maximum C-specific N transport rate (gN (gC)-1 d-1)
TNmax_Phy = umax_Phy * NCmax_Phy;

# Phytoplankton C-specific N transport rate (gN (gC)-1 d-1)
NCt_Phy = TNmax_Phy * x(1) / (x(1) + ktAM_Phy);

# N-specific growth rate (gN (gN)-1 d-1)
uN_Phy(t - 1) = NCt_Phy / NC_Phy(t -1);

# Phytoplankton population uptake of ammonium-N (ugN L-1 d-1)
Nup_Phy = x(3) * NCt_Phy;

# Growth rate in phytoplankton-C (ugC L-1 d-1)
groC_Phy = x(3) * u_Phy(t - 1);

## State equations
# Ammonium
xdot(1, 1) = in_out_Am - Nup_Phy;

# Phytoplankton-N
xdot(1, 2) = Nup_Phy - outN_Phy;

# Phytoplankton-C
xdot(1, 3) = groC_Phy - outC_Phy;

# System
xdot(1, 4) = xdot(1, 1) + xdot(1, 2);

endfunction

