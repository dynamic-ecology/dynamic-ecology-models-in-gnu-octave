## GNU Octave ports of the models in "Dynamic Ecology - an introduction to
## the art of simulating trophic dynamics" by Flynn, K. (2018).
## Copyright (C) 2020  Ekin Akoglu and Kevin J. Flynn
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <https://www.gnu.org/licenses/>.

clear;

# Simulation time frame
t0 = 0;      # start time
tfinal = 50; # end time
stepsize = 0.0625;
tspan = (t0:stepsize:tfinal); # time span

# Initial conditions
Prey =	10; # Prey population size (Prey nos)
Pred = 1;   # Predator population size (Pred nos)
# Initial conditions array
x0 = [Prey, Pred];

# Simulate
y = rk4(@func_LV, tspan, stepsize, x0);

# Plot the results
h = figure;

subplot(1, 2, 1);
plot(tspan, y(:, 1), 'r', tspan, y(:, 2), 'g');
set(gca,'FontSize',12);
xlabel('Time (d)', 'FontSize', 12);
ylabel('nos', 'FontSize', 12);
hleg = legend('Prey', 'Pred');
set(hleg, 'FontSize', 8);

subplot(1, 2, 2);
plot(y(:, 1), y(:, 2), 'r');
set(gca,'FontSize',12);
xlabel('Prey', 'FontSize', 12);
ylabel('Pred', 'FontSize', 12);

print(h, 'Chapter-6-Lotka-Volterra-Model.png', '-dpng', '-color');