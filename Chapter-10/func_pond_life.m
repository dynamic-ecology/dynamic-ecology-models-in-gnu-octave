## GNU Octave ports of the models in "Dynamic Ecology - an introduction to
## the art of simulating trophic dynamics" by Flynn, K. (2018).
## Copyright (C) 2020  Ekin Akoglu and Kevin J. Flynn
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <https://www.gnu.org/licenses/>.

function xdot = func_pond_life(t, x)

global tspan
global g_Light
global g_W
global gRH
global gT_air
global spillout
global inflow_Pond
global seep_Pond
global evap_Pond
global z_Pond
global z_spill
global zOp_spill
global DIN
global umax_Phy
global umax_Zoo
global u_Phy
global u_Zoo

# Physical parameters
T_air = 20;         # Air temperature (Celsius)
T_inflow = 6;       # Inflow water temperature (Celsius)
Tini = 10;          # Initial pond temperature (Celsius)
salinity = 0;       # Salinity (dl)
RH = 30;            # Relative humidity
SA_Pond = 100;      # Surface area of pond (m2)
wid_spill = 0.05;   # Width of spillway (m)
z_spill = 0.3;      # Height of spill way lip above lowest point of pond (m)
SeepR = 0.025;      # Seepage rate of water from the pond related to SA and per m of depth (d-1)
SBconst = 5.73E-08; # W/m2/ K4 Stefan-Boltzmann constant
SDA = 0.3;          # Specific Dynamic Action (dl)
bkRad1 = 0.05;      # Back radiation constant (1 / mb^0.5)
bkRad2 = 0.35;      # Back radiation constant (dl)
cloud = 2;          # Cloud cover (0 to 8) (oktas)
con_fact = 4.57;    # Correction factor to converts light as W / m2 to PAR (umol s-1 W-1)
cp = 4186;          # Specific heat of water (J/kg * C)
Emissivity = 0.985; #	Emissivity of thermal radiation (dl)
g = 9.81;           # Acceleration due to gravity (m s-2)
W = 10;             # Wind speed (m s-1)

# Nutrient parameters
N_inflow = 100 * 14; # Inflow concentration of N into pond (mgN m-3)

# Phytoplankton parameters
attco_W = 0.1;        # Absorbance coefficient for growth medium (water) (m-1)
abco_Chl = 0.02;      # Light absorbance coefficient for chlorophyll (m2 (mgChl)-1)
BR_Phy = 0.1;         # Phytoplankton basal respiration as proportion of umax_Phy (dl)
alpha_Chl = 7.00E-06; # Slope of Chl-specific PE curve 	(m2 g-1 chl.a)*(gC umol-1 photon)
ChlC_Phy = 0.06;      # Mass ratio content of chlorophyll:C in the phytoplankton (gChl (gC)-1)
inflow_Phy = 10 * 14; # Concentration of incoming phytoplankton biomass (mgN m-3)
kN_Phy = 1 * 14;      # Half saturation constant for u_Phy (mgN m-3)
NC_Phy = 0.15;        # Mass ratio content of N-biomass:C in the phytoplankton (gN (gC)-1)
Q10_Phy = 1.8;        # Phytoplankton Q10 (dl)
Tref_Phy = 10;        # Reference temperature for phytoplankton growth (Celsius)
Uref_Phy = 0.693;     # Phytoplankton maximum growth rate at reference T (d-1)

# Zooplankton parameters
AEN_Zoo = 0.6;     # Assimilation efficiency (dl)
BR_Zoo = 0.2;      # Zooplankton basal respiration rate proportioned to umax_Zoo (dl)
kPhy_Zoo = 14 * 5; # Half saturation constant for zooplankton predation on phytoplankton (mgN m-3)
Q10_Zoo = 2.2;     # Zooplankton Q10 (dl)
Tref_Zoo = 10;     # Reference temperature for zooplankton growth (Celsius)
Uref_Zoo = 1.5;    # Zooplankton maximum growth rate at reference T (d-1)

flag = 0; # 0 = fixed; 1 = data input flag between fixed or data input values 

## Auxiliaries
# Inflow of water (m3 d-1)
if tspan(t - 1) < 10
  In = 2 * tspan(t - 1) + 0.1;
else
  In = 0.1;
endif
# Inflow of water (m3 d-1)
inflow_Pond(t - 1) = In;
# Effective dilution rate of pond (d-1)
dil = inflow_Pond(t - 1) / x(5);

if flag == 0
  mult1 = 1;
  mult2 = 0;
else
  mult1 = 0;
  mult2 = 1;
endif
# Operational air temperature (Celsius)
op_Tair = mult1 * T_air + mult2 * gT_air(t - 1);

if flag == 0
  mult1 = 1;
  mult2 = 0;
else
  mult1 = 0;
  mult2 = 1;
endif
# Operational relative humidity
op_RH = mult1 * RH + mult2 * gRH(t - 1);
# Water vapour pressure in the atmosphere (mb)
ea = (op_RH / 100) * 6.11 * 10^(7.5  *  op_Tair / (op_Tair + 237));
# Back radiation (W m-2)
Q_br = (1 - 0.1 * cloud) * Emissivity * SBconst * (bkRad2 - bkRad1 * sqrt(ea)) * (x(4) + 273)^4;
# Saturated vapour pressure (mb)
es = 6.11  *  10^(7.5  *  x(4) / (x(4) + 237));

if flag == 0
  mult1 = 1;
  mult2 = 0;
else
  mult1 = 0;
  mult2 = 1;
endif
# Operational wind speed (m s-1)
Wind = mult1 * W + mult2 * g_W(t - 1);

# Cooling evaporative heat flux (W m-2)
Qe = (3.8 * (es - ea) * Wind);
# Sensible heat flux from pond (W m-2)
Qh = 2.5 * (x(4) - op_Tair) * Wind;

if tspan(t - 1) - floor(tspan(t - 1)) < 0.5
  Light = 300;
else
  Light = 0;
endif

if flag == 0
  mult1 = 1;
else
  mult1 = 0;
endif
if flag == 1 && g_Light(t - 1) > 0
  mult2 = 1;
else
  mult2 = 0;
endif
# Light at the pond surface (W m-2)
Wm2 = mult1 * Light + mult2 * g_Light(t - 1);
# Net heat flux (W m-2)
Qn = Wm2 - (Q_br + Qe + Qh);
 # Water density (kg m-3)
rho = 1000 + salinity;

# Depth of pond water (m)
if x(5) > 0
  z_Pond(t - 1) = x(5) / SA_Pond;
else
  z_Pond(t - 1) = 0;
endif

# Rate of change of temperature due to heating and cooling (Celsius d-1)
dTwIn = (Qn / (cp  *  rho  *  z_Pond(t - 1))) * 60  *  60  *  24;
# Latent heat of water evaporation (J kg-1)
LH = 1000 * (2500.8 - 2.36 * x(4) + 0.0016 * x(4)^2 - 0.00006 * x(4)^3);
# Evaporation rate (m s-1)
er = Qe / (LH  *  rho);
# Loss of water through evaporation (m3 d-1)
if x(5) > 0.1
  evap_Pond(t - 1) = (er * 60 * 60 * 24) * SA_Pond;
else
  evap_Pond(t - 1) = 0;
endif

# Phytoplankton biomass concentration (umolN L-1)
Phy = (x(1) / x(5)) / 14;
# Phytoplankton-N specific coefficient for light absorbance (m2 (mgN)-1)
abco_PhyN = abco_Chl * ChlC_Phy / NC_Phy;
# Specific slope of PE curve ((m2)*(umol-1 photon))
alpha_u = alpha_Chl * ChlC_Phy;
# Attenuation coefficient to phytoplankton N-biomass (m-1)
attco_Phy = abco_PhyN * Phy;
# Total attenuation (dl)
att_tot = z_Pond(t - 1) * (attco_W + attco_Phy);

# Negative exponent of total attenuation (dl)
exatt = exp(-att_tot);


# Concentration of nutrient-N (uM)
DIN(t - 1) = (x(2) / x(5)) / 14;

# Inflow of N into pond (mgN d-1)
inflow_N = inflow_Pond(t - 1) * N_inflow;
# Incoming phytoplankton; this also serves to inoculate the system (mgN d-1)
inflow_Phyto = inflow_Phy * inflow_Pond(t - 1);
# Temperature adjusted zooplankton maximum growth rate (d-1)
umax_Zoo(t - 1) = Uref_Zoo * Q10_Zoo^((x(4) - Tref_Zoo) / 10);
# Maximum ingestion rate, allowing u_Zoo=umax_Zoo under optimal conditions (d-1)
ingNmax_Zoo = (umax_Zoo(t - 1) * (1 + BR_Zoo)) / (AEN_Zoo * (1 - SDA));
# Ingestion rate of phytoplankton (d-1)
ingN_Zoo = ingNmax_Zoo * Phy / (Phy + kPhy_Zoo);
# Zooplankton growth rate (d-1)
u_Zoo(t - 1) = ingN_Zoo * AEN_Zoo * (1 - SDA)-(umax_Zoo(t - 1) * BR_Zoo); 
# Assimilation rate (d-1)
assN_Zoo = ingN_Zoo * AEN_Zoo;

# Loss of phytoplankton through ingestion by zooplankton (mgN d-1)
ingN = x(3) * ingN_Zoo;
# PFD at surface (umoles m-2 s-1)
nat_PFD = Wm2 * con_fact;
# Regeneration of N by zooplankton as a consequence of grazing and respiration (mgN d-1) 
Nregen = x(3) * (umax_Zoo(t - 1) * BR_Zoo) + assN_Zoo * SDA + ingN * (1 - AEN_Zoo);
# Index of N-limitation for phytoplankton growth (dl)
Nu = DIN(t - 1) / (DIN(t - 1) + kN_Phy);

# Temperature adjusted phytoplankton maximum growth rate (d-1)
umax_Phy(t - 1) = Uref_Phy * Q10_Phy^((x(4) - Tref_Phy) / 10);
# Maximum photosynthetic rate to balance BR_Phy to give u_Phy=umax_Phy (d-1)
PSmax = umax_Phy(t - 1) * (1 + BR_Phy);
# Maximum photosynthetic rate down-regulated in consequence of nutrient stress (d-1)
PSqmax = PSmax * Nu;
# Intermediate in depth-integrated photosynthesis rate (d)
pytq = (alpha_u * nat_PFD * 24 * 60 * 60) / PSqmax;
# Phytoplankton N-specific growth rate (d-1)
PSqz = PSqmax * (log(pytq + sqrt(1 + pytq^2)) - log(pytq * exatt + sqrt(1 + (pytq * exatt)^2))) / att_tot;
# Phytoplankton growth rate (d-1)
u_Phy(t - 1) = PSqz - (umax_Phy(t - 1) * BR_Phy);
# Phytoplankton biomass growth (mgN d-1)
gro_Phy = x(1)  *  u_Phy(t - 1);

# Depth of water over spillway (m)
if z_Pond(t - 1) > z_spill;
  zOp_spill(t - 1) = z_Pond(t - 1) - z_spill;
else
  zOp_spill(t - 1) = 0;
endif
# Area of the mouth of the spillway (m2)
XSA_spill = zOp_spill(t - 1) * wid_spill;

# Loss of water through overflow through a spillway (m3 d-1)
spillout(t - 1) = 60 * 60 * 24 * XSA_spill * (2 * g * zOp_spill(t - 1))^0.5;

# Seepage loss of water (m3 d-1)
if z_Pond(t - 1) > 0
  seep_Pond(t - 1) = SeepR * SA_Pond * z_Pond(t - 1);
else
  seep_Pond(t - 1) = 0;
endif
# Loss of nutrient-N through seepage (mgN d-1)
seep_N = x(2) * seep_Pond(t - 1) / x(5);

# Loss of nutrient-N over the spillway (mgN d-1)
spill_N = x(2) * spillout(t - 1) / x(5);

# Loss of phytoplankton biomass over the spillway (mgN d-1)
spill_Phy = x(1) * spillout(t - 1) / x(5);

# Loss of zooplankton biomass over the spillway (mgN d-1)
spill_Zoo = x(3) * spillout(t - 1) / x(5);

# Specific dilution rate as would apply to phytoplankton (d-1)
spilld = spillout(t - 1) / x(5);

# Stop command to halt simulation when water attains a minimum depth (dl)
if z_Pond(t - 1)< 0.01
  Stop_z = 1;
  error("Minimum depth is attained! Simulation stopped!");
endif

# Change in water temperature with incoming water (Celsius d-1)
T_dil = (T_inflow - x(4)) * dil;

# Zooplankton biomass concentration (umolN L-1)
Zoo(t - 1) = (x(3) / x(5)) / 14; 

## State equations
# Phytoplankton
xdot(1, 1) = gro_Phy + inflow_Phyto - ingN - spill_Phy;

# Pond nutrient-N
xdot(1, 2) = inflow_N + Nregen - gro_Phy - seep_N - spill_N;

# Zooplankton
xdot(1, 3) = ingN - Nregen - spill_Zoo;

# Temperature of pond water
xdot(1, 4) = dTwIn + T_dil;

# Pond volume
xdot(1, 5) = inflow_Pond(t - 1) - evap_Pond(t - 1) - seep_Pond(t - 1) - spillout(t - 1);

endfunction

