## GNU Octave ports of the models in "Dynamic Ecology - an introduction to
## the art of simulating trophic dynamics" by Flynn, K. (2018).
## Copyright (C) 2020  Ekin Akoglu and Kevin J. Flynn
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <https://www.gnu.org/licenses/>.

clear;

global NC_Phy
global NC_Zoo
global u_Phy
global u_Zoo
global uN_Phy
global NCu_Phy
global AEmin
global AEmax
global AEqual
global AEquan
global AEC_Zoo
global ingC_Zoo
global ingCmax_Zoo
global AEC
global AEN
global GGEC
global GGEN
global palat_Phy
global NCPhy_Zoo
global Stoich_con
global XSC

# Simulation time frame
t0 = 0;      # start time
tfinal = 100; # end time
stepsize = 0.015625;
tspan = (t0:stepsize:tfinal); # time span

# Preallocate global arrays for speed
NC_Phy = zeros(1, length(tspan)-1);
NC_VO = zeros(1, length(tspan)-1);
u_Phy = zeros(1, length(tspan)-1);
u_Zoo = zeros(1, length(tspan)-1);
uN_Phy = zeros(1, length(tspan)-1);
NCu_Phy = zeros(1, length(tspan)-1);
AEC_Zoo = zeros(1, length(tspan)-1);
ingC_Zoo = zeros(1, length(tspan)-1);
ingCmax_Zoo = zeros(1, length(tspan)-1);
AEC = zeros(1, length(tspan)-1);
AEN = zeros(1, length(tspan)-1);
GGEC = zeros(1, length(tspan)-1);
GGEN = zeros(1, length(tspan)-1);
palat_Phy = zeros(1, length(tspan)-1);
NCPhy_Zoo= zeros(1, length(tspan)-1);
Stoich_con = zeros(1, length(tspan)-1);

# Initial conditions
Am = 14 * 10;         # Ammonium-N (ugN L-1)
C_Phy = 12;           # Phytoplankton-C (ugC L-1) 
N_Phy = C_Phy * 0.05; # Phytoplankton-N (ugN L-1)
C_Zoo = 1;            # Zooplankton C-biomass (ugC L-1)
VON = 0;              # Faecal material-C (ugC L-1)
VOC = 0;              # Faecal material-N (ugN L-1)
sysN	= Am + N_Phy + C_Zoo * 0.2 + VON;   # System N (ugN L-1)
# Initial conditions arrayfun
x0 = [Am, N_Phy, C_Phy, C_Zoo, VON, VOC, sysN];

# Simulate
y = solver(@func_quota_npz, tspan, stepsize, x0);

# Plot the results
h = figure;

subplot(3, 2, 1)
plot(tspan, y(:, 7), 'r', tspan, y(:, 1), 'g', tspan, y(:, 2), 'b', tspan, y(:, 4) * 0.2, 'k', tspan, y(:, 5), 'm');
set(gca,'FontSize',12);
ylim([0 150]);
xlabel('Time (d)', 'FontSize', 12);
ylabel('\mugN L^{-1}', 'FontSize', 12);
hleg = legend('sysN', 'Am', 'N\_Phy', 'N\_Zoo', 'VON', 'location', 'eastoutside');
set(hleg, 'FontSize', 8);

subplot(3, 2, 2)
plot(tspan, y(:, 3), 'r', tspan, y(:, 4), 'g');
set(gca,'FontSize',12);
xlabel('Time (d)', 'FontSize', 12);
ylabel('\mugC L^{-1}', 'FontSize', 12);
hleg = legend('C\_Phy', 'C\_Zoo');
set(hleg, 'FontSize', 8);

subplot(3, 2, 3)
plot(tspan(2:end), u_Phy, 'r', tspan(2:end), u_Zoo, 'g');
set(gca,'FontSize',12);
xlabel('Time (d)', 'FontSize', 12);
hleg = legend('u\_Phy', 'u\_Zoo', 'location', 'eastoutside');
set(hleg, 'FontSize', 8);

subplot(3, 2, 4)
plot(tspan(2:end), NC_Phy, 'r', tspan(2:end), repmat(NC_Zoo, 1, length(tspan)-1), 'g', tspan(2:end), (y(2:end, 5) ./ y(2:end, 6)), 'b');
set(gca,'FontSize',12);
xlabel('Time (d)', 'FontSize', 12);
ylabel('N:C', 'FontSize', 12);
hleg = legend('NC\_Phy', 'NC\_Zoo', 'NC\_VO', 'location', 'eastoutside');
set(hleg, 'FontSize', 8);

subplot(3, 2, 5)
plot(y(2:end, 1), u_Phy, 'r');
set(gca,'FontSize',12);
xlabel('Am', 'FontSize', 12);
ylabel('u\_Phy', 'FontSize', 12);

subplot(3, 2, 6)
plot(y(2:end, 1), uN_Phy, 'r');
set(gca,'FontSize',12);
xlabel('Am', 'FontSize', 12);
ylabel('uN\_Phy', 'FontSize', 12);

print(h, 'Chapter-16-Quota-NPZ_1.png', '-dpng', '-color');

h2 = figure;

subplot(3, 2, 1);
plot(tspan(2:end), AEqual, 'r', tspan(2:end), repmat(AEmin, 1, length(tspan)-1), 'g', tspan(2:end), repmat(AEmax, 1, length(tspan)-1), 'b', tspan(2:end), AEC_Zoo, 'k');
set(gca,'FontSize',12);
xlabel('Time (d)', 'FontSize', 12);
hleg = legend('AEqual', 'AEmin', 'AEmax', 'AEC\_Zoo', 'location', 'eastoutside');
set(hleg, 'FontSize', 8);

subplot(3, 2, 2);
plot(tspan(2:end), ingCmax_Zoo, 'r', tspan(2:end), ingC_Zoo, 'g');
set(gca,'FontSize',12);
xlabel('Time (d)', 'FontSize', 12);
hleg = legend('ingCmax\_Zoo', 'ingC\_Zoo', 'location', 'eastoutside');
set(hleg, 'FontSize', 8);

subplot(3, 2, 3);
plot(tspan(2:end), AEC, 'r', tspan(2:end), AEN, 'g');
set(gca,'FontSize',12);
xlabel('Time (d)', 'FontSize', 12);
hleg = legend('AEC', 'AEN', 'location', 'eastoutside');
set(hleg, 'FontSize', 8);

subplot(3, 2, 4);
plot(tspan(2:end), GGEC, 'r', tspan(2:end), GGEN, 'g');
set(gca,'FontSize',12);
xlabel('Time (d)', 'FontSize', 12);
hleg = legend('GGEC', 'GGEN', 'location', 'eastoutside');
set(hleg, 'FontSize', 8);

subplot(3, 2, 5);
plot(NCu_Phy, palat_Phy, 'r');
set(gca,'FontSize',12);
xlabel('NCu\_Phy', 'FontSize', 12);
ylabel('palat\_Phy', 'FontSize', 12);

subplot(3, 2, 6);
plot(tspan(2:end), palat_Phy, 'r');
set(gca,'FontSize',12);
xlabel('Time (d)', 'FontSize', 12);
ylabel('palat\_Phy', 'FontSize', 12);

set(gcf, 'PaperPosition', [0.25000 2.50000 9.00000 12.00000]);
print(h2, 'Chapter-16-Quota-NPZ_2.png', '-dpng', '-color');

h3 = figure;

subplot(3, 2, 1);
plot(y(2:end, 3), ingC_Zoo, 'r');
set(gca,'FontSize',12);
xlabel('C\_Phy', 'FontSize', 12);
ylabel('ingC\_Zoo', 'FontSize', 12);

subplot(3, 2, 2);
plot(y(2:end, 3), u_Zoo, 'r');
set(gca,'FontSize',12);
xlabel('C\_Phy', 'FontSize', 12);
ylabel('u\_Zoo', 'FontSize', 12);

subplot(3, 2, 3);
plot(y(:, 3), y(:, 4), 'r');
set(gca,'FontSize',12);
xlabel('C\_Phy', 'FontSize', 12);
ylabel('C\_Zoo', 'FontSize', 12);

subplot(3, 2, 4);
plot(NC_Phy, ingCmax_Zoo, 'r');
set(gca,'FontSize',12);
xlabel('NC\_Phy', 'FontSize', 12);
ylabel('ingCmax\_Zoo', 'FontSize', 12);

subplot(3, 2, 5);
plot(tspan(2:end), NCPhy_Zoo, 'r', tspan(2:end), Stoich_con, 'g');
set(gca,'FontSize',12);
ylabel('Time (d)');
hleg = legend('NCPhy\_Zoo', 'Stoich\_con', 'location', 'eastoutside');
set(hleg, 'FontSize', 8);

subplot(3, 2, 6);
plot(NC_Phy, NCu_Phy, 'r');
set(gca,'FontSize',12);
xlim([0.05 0.14]);
xlabel('NC\_Phy', 'FontSize', 12);
ylabel('NCu\_Phy', 'FontSize', 12);

set(gcf, 'PaperPosition', [0.25000 2.50000 9.00000 12.00000]);
print(h3, 'Chapter-16-Quota-NPZ_3.png', '-dpng', '-color');

h4 = figure;

subplot(3, 2, 1);
plot(NC_Phy, ingC_Zoo, 'r');
set(gca,'FontSize',12);
xlabel('NC\_Phy', 'FontSize', 12);
ylabel('ingC\_Zoo', 'FontSize', 12);

subplot(3, 2, 2);
plot(tspan(2:end), AEquan, 'r', tspan(2:end), palat_Phy, 'g');
set(gca,'FontSize',12);
ylim([0 2]);
xlabel('Time (d)', 'FontSize', 12);
ylabel('Index', 'FontSize', 12);
hleg = legend('AEquan', 'palat\_Phy', 'location', 'eastoutside');
set(hleg, 'FontSize', 8);

subplot(3, 2, 3);
plot(tspan(2:end), AEquan, 'r');
set(gca,'FontSize',12);
xlabel('Time (d)', 'FontSize', 12);
ylabel('Aequan', 'FontSize', 12);

subplot(3, 2, 4);
plot(tspan(2:end), u_Phy, 'r', tspan(2:end), uN_Phy, 'g', tspan(2:end), NCu_Phy, 'b');
set(gca,'FontSize',12);
xlabel('Time (d)', 'FontSize', 12);
hleg = legend('u\_Phy', 'uN\_Phy', 'NCu\_Phy', 'location', 'eastoutside');
set(hleg, 'FontSize', 8);

subplot(3, 2, 5);
plot(tspan(2:end), XSC, 'r');
set(gca,'FontSize',12);
xlabel('Time (d)', 'FontSize', 12);
ylabel('XSC', 'FontSize', 12);

set(gcf, 'PaperPosition', [0.25000 2.50000 9.00000 12.00000]);
print(h4, 'Chapter-16-Quota-NPZ_4.png', '-dpng', '-color');