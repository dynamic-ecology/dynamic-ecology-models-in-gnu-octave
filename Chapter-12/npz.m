## GNU Octave ports of the models in "Dynamic Ecology - an introduction to
## the art of simulating trophic dynamics" by Flynn, K. (2018).
## Copyright (C) 2020  Ekin Akoglu and Kevin J. Flynn
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <https://www.gnu.org/licenses/>.

clear;

global tspan
global mix_dep
global H
global tot_mix
global PSqz
global Nu
global psu
global G
global Zu
global AP
global Rate_1
global Rate_2

data = data = dlmread('PowerSim.csv', ",", [1, 0, 11681, 3]);

mix_dep = data(:, 2);  # Mixed layer depth against Julian date (m)
H = data(:, 3);        # Rate of change of mixed layer depth (m d-1)

# Simulation time frame
t0 = 0;      # start time
tfinal = 365; # end time
stepsize = 0.03125;
tspan = (t0:stepsize:tfinal); # time span

# Preallocate global arrays for speed
tot_mix = zeros(1, length(tspan)-1);
PSqz = zeros(1, length(tspan)-1);
Nu = zeros(1, length(tspan)-1);
psu = zeros(1, length(tspan)-1);
G = zeros(1, length(tspan)-1);
Zu = zeros(1, length(tspan)-1);
AP = zeros(1, length(tspan)-1);
Rate_1 = zeros(1, length(tspan)-1);
Rate_2 = zeros(1, length(tspan)-1);

# Initial conditions
N = 5.6;        # Dissolved inorganic-N (nitrate and ammonium) (mmolN m-3)
P = 0.09;       # Phytoplankton biomass (mmolN m-3)
Z = 0.029;      # Zooplankton biomass (mmolN m-3)
Corpse = 0;     # Corpse N-biomass lost from system; records cumulative loss (mmolN m-3)
Pellets = 0;    # Zooplankton faecal pellets; records cumulative loss (mmolN m-3)
cum_prod = 0;   # Cummulative primary production (mmolN m-2 d-1)
DAY_avg_AP = 0; # Day-averaged areal primary production (mmolN m-2 d-1)
# Initial conditions array
x0 = [N P Z Corpse Pellets cum_prod DAY_avg_AP];

# Simulate
y = solver(@func_npz, tspan, stepsize, x0);

# Plot the results
h = figure;

subplot(4, 2, 1);
plot(tspan, y(:, 1), 'r', tspan, y(:, 2), 'g', tspan, y(:, 3), 'b');
set(gca,'FontSize',12);
xlabel('Julian date (d)', 'FontSize', 12);
ylabel('mmol N m^{3}', 'FontSize', 12);
hleg = legend('N', 'P', 'Z', 'location', 'eastoutside');
set(hleg, 'FontSize', 8);
ylim([0 7]);
xlim([0 365]);

subplot(4, 2, 2);
plot(tspan, y(:, 2), 'g', tspan, y(:, 3), 'b');
set(gca,'FontSize',12);
xlabel('Julian date (d)', 'FontSize', 12);
ylabel('mmol N m^{3}', 'FontSize', 12);
hleg = legend('P', 'Z', 'location', 'eastoutside');
set(hleg, 'FontSize', 8);
ylim([0 3]);
xlim([0 365]);

subplot(4, 2, 3);
plot(tspan(2:end), PSqz, 'r');
set(gca,'FontSize',12);
xlabel('Julian date (d)', 'FontSize', 12);
ylabel('PSqz (d^{-1})', 'FontSize', 12);
ylim([0 0.5]);
xlim([0 365]);

subplot(4, 2, 4);
plot(tspan, y(:, 7), 'r');
set(gca,'FontSize',12);
xlabel('Julian date (d)', 'FontSize', 12);
ylabel('Production (mmol N m^{-2} d^{-1})', 'FontSize', 12);
xlim([0 365]);

subplot(4, 2, 5);
plot(tspan(2:end), Nu, 'r', tspan(2:end), psu, 'g');
set(gca,'FontSize',12);
xlabel('Julian date (d)', 'FontSize', 12);
ylabel('Quotient', 'FontSize', 12);
hleg = legend('Nu', 'psu', 'location', 'eastoutside');
set(hleg, 'FontSize', 8);
xlim([0 365]);

subplot(4, 2, 6);
plot(tspan(2:end), G, 'r', tspan(2:end), Zu, 'g');
set(gca,'FontSize',12);
xlabel('Julian date (d)', 'FontSize', 12);
ylabel('d^{-1}', 'FontSize', 12);
hleg = legend('G', 'Zu', 'location', 'eastoutside');
set(hleg, 'FontSize', 8);
xlim([0 365]);

subplot(4, 2, 7);
plot(y(:, 1), y(:, 2));
set(gca,'FontSize',12);
xlabel('N', 'FontSize', 12);
ylabel('P', 'FontSize', 12);

subplot(4, 2, 8);
plot(y(:, 2), y(:, 3));
set(gca,'FontSize',12);
xlabel('P', 'FontSize', 12);
ylabel('Z', 'FontSize', 12);

set(gcf, 'PaperPosition', [0.25000 2.50000 9.00000 12.00000]);
print(h, 'Chapter-12-NPZ_1.png', '-dpng', '-color');

h2 = figure;

subplot(2, 2, 1);
plot(tspan(2:end), tot_mix, 'r');
set(gca,'FontSize',12);
xlabel('Julian date (d)', 'FontSize', 12);
ylabel('tot\_mix (d^{-1})', 'FontSize', 12);
set(hleg, 'FontSize', 8);
xlim([0 365]);

subplot(2, 2, 2);
plot(tspan(2:end), AP, 'r');
set(gca,'FontSize',12);
xlabel('Julian date (d)', 'FontSize', 12);
ylabel('AP', 'FontSize', 12);
set(hleg, 'FontSize', 8);
xlim([0 365]);

subplot(2, 2, 3);
plot(tspan, mix_dep, 'r');
set(gca,'FontSize',12);
xlabel('Julian date (d)', 'FontSize', 12);
ylabel('mix\_dep (m)', 'FontSize', 12);
xlim([0 365]);

subplot(2, 2, 4);
plot(tspan, H, 'r');
set(gca,'FontSize',12);
xlabel('Julian date (d)', 'FontSize', 12);
ylabel('H (m d^{-1})', 'FontSize', 12);
xlim([0 365]);

print(h2, 'Chapter-12-NPZ_2.png', '-dpng', '-color');

